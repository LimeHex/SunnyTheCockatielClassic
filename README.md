# Sunny The Cockatiel! Classic

<img src="STC!C_Icon.svg" alt="Sunny The Cockatiel Classic Icon" width="100"/>

Sunny The Cockatiel! Classic is a side-game in the Sunny series, and a half-sequel to BIRBOUT!.
Design-wise it's based on the 2018 build of the original Unity Sunny The Cockatiel, but built in a much newer version of Godot Engine.

## Building

`git clone` this repo into a folder of your choosing. From there, open the project.godot file in the latest build of Godot 4. 
You might need to export templates for your platform of choice.

Only Linux has been tested as of now, and official Windows builds are only included as part of release builds

## Credit

Be sure to credit me (Jazztache) if you make forks.
