class_name RotSca extends Node2D

# RotSca.gd - Manages rotation of the head and scaling of the body, basically the hardcoded animations.
# Jazztache - 1733Thu08Feb24

## Hardcoded animations. This includes character flipping, and head rotating!

# NOTE: This is a seperate script, because Firing nor Movement was a good place to put it.
# TODO: Make Sunny stretch if in fast-fall. Like the falling animation in Ring Racers.


# == VARIABLES ==
@onready var root = $"../.." ## Get the root for scaling
@onready var headPivot = $"../../BodyParts/HeadPivot" ## The head's dedicated pivot point node.

var previousFlip = 1
var facingDirection = 1 ## The direction Sunny is facing. Used for correcting head rotation.

var HEAD_ROTATION_INTENSITY = 1000
# == FUNCTIONS ==

## Flip the character to face the other way
func flipCharacter(direction):
	# - Calc Flip Direction -

	if direction == 1:
		facingDirection = 1
	elif direction == -1:
		facingDirection = -1
	# - Apply Flip Direction -
	# Apply scale doesn't flip Sunny upside down.
	if facingDirection != previousFlip:
		root.apply_scale(Vector2(-1,1)) # Only flip on the X axis # [1] <1858Thu08Feb24> (Forgot to reference it)
		previousFlip = facingDirection

## The function for setting the head rotation
func rotateHead(aimVector):
	# - Head Rotation -
	var angleAsVector = Vector2(aimVector.x * -facingDirection,-aimVector.y) # Make sure that body flipping and Godot's co-ordinate systems are taken into account when adjusting the angle. <2017Sun25Feb24>
	var angleInRadians = angleAsVector.angle() # [2] Get the angle in radians
	headPivot.rotation = angleInRadians # Rotate the head towards it.

# == SIGNALS ==

# NOTE: Due to this being connected from arbritiary components, (Moving on Plovers & Head Movement on Sunny), connections are made in other scripts.

# == REFERENCES ==
# [1] [https://forum.godotengine.org/t/i-want-to-flip-my-character-on-the-horizontal-axis-but-whats-the-best-way/33830] [1858Thu08Feb24]
# [2] [https://docs.godotengine.org/en/stable/classes/class_vector2.html#class-vector2-method-from-angle]
