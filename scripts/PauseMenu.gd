class_name PauseMenu extends Control

# PauseMenu.gd - Used to make sure the game can pause
# Written by Jazztache - 1244Thu08Feb24

## The scripting logic for the pause menu.

# == VARIABLES & OBJECTS ==
# - Scene References -
@export var gameMaster: GameMaster
@export var resumeButton : Button
@export var retryButton : Button
@export var quitToTitleButton : Button
@export var quitToDesktopButton : Button
var masterRoot ## The very root of the scene, even higher than the world and the game master. Useful for pausing and console 'states'

# == FUNCTIONS ==
# - Boilerplate -
## The usual Godot boilerplate.
func _ready():
	findMasterScene()
	connectSignals()
	hide()

# - Show/Hide -
## Show and Hide for the pause menu when paused. NOTE: The actual pausing takes place in GameMaster.gd
func onGameManagerTogglePaused(isPaused : bool):
	if isPaused:
		show()
	else:
		hide()

# - Node Stuff -

## Get a reference to the master scene
func findMasterScene():
	masterRoot = get_node("/root/MasterRoot") # Get the master scene node for quitting to title.

# - Menu Functions -

## Resume gameplay.
func resume():
	# Just directly set the variable.
	print("[PauseMenu] Unpausing game...")
	masterRoot.paused = false # Tell the master root to unpause the game

## Retry the level. Tell the gameMaster to start a new game.
func retry():
	print("[PauseMenu] Retrying level...")
	# NOTE: This just sends the reset signal immediately to the GameMaster
	gameMaster.startGame(false)
	masterRoot.paused = false # If you don't put this here, the game stays paused, which is not what we want.
	pass

## Quit the game to the title screen
func quitToTitle():
	print("[PauseMenu] Quitting to title...")
	masterRoot.loadMainMenu() # Load the main menu from the master scene.
	pass

## Quit the game to desktop
func quitToDesktop():
	print("[PauseMenu] Quitting to desktop...")
	get_tree().quit()

# - Signal Conncetions -

## Connect signals, in this case the buttons on the pause menu to the functions listed above.
func connectSignals():
	masterRoot.togglePause.connect(onGameManagerTogglePaused)
	resumeButton.pressed.connect(resume)
	retryButton.pressed.connect(retry)
	quitToTitleButton.pressed.connect(quitToTitle)
	quitToDesktopButton.pressed.connect(quitToDesktop)

# == REFERENCES ==
#[1] [https://odysee.com/@ChrisTutorials:b/how-to-make-a-pause-game-menu-pause-the:1?t=37] <1244Thu08Feb24>
