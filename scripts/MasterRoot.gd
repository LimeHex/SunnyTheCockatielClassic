# MasterRoot.gd
# Written by jazztache
# 1429Tue01Aug23

class_name MasterRoot extends Node

## The script in charge of the entire game's initialisation.

# = VARIABLES =
# - PreLoaded Objects -
@onready var mainMenu: PackedScene = preload("res://scenes/main/MainMenu.tscn") ## The scene for the main menu.
@onready var gameScene: PackedScene = preload("res://scenes/main/GameScene.tscn") ## The scene for the main gameplay.
@onready var alphaPopup: PackedScene = preload("res://scenes/ui/DisclaimerPopup.tscn") ## The pop-up disclaimer for alpha builds.
# - Module Usage -
@onready var modules: Node = $Modules ## This is the node for explicit modules.
@onready var console: PackedScene = preload("res://modules/STC!CConsole/scenes/STC!CConsoleMain.tscn") ## The developer Console. Always starts disabled. 
@onready var screenshots: PackedScene = preload("res://modules/STC!CScreenshots/scenes/STC!CScreenshotsMain.tscn") ## The screenshots module.
# - Level Selection -
var currentLevel : String = "unset" ## The  urrent selectwed level. Used for beta and Single Level.
# - Pause/Menu States -
var consoleOpen ## Check wether the console is open
var isMainMenu ## Check wether we are on the main menu for pausing, but NOT for console.
# - Pause Variables -
var paused : bool = false: ## Wether the game is paused or not
	get:
		return paused
	set(value):
		paused = value
		get_tree().paused = value
		togglePause.emit(paused) # Diverges from the tutorial, I remember using this primarily.
# - Referenced Objects - 
var mainMenuInstance: Node ## Used to store the main menu instance.
var consoleInstance: Node ## Used to store the console. <1455Sun11Feb24>
var screenshotsInstance: Node ## used to store the screenshots module. <1226Thu14Mar24>

# == FUNCTIONS ==
# - Boilerplate -

## Load the main menu on startup
func _ready():
	loadMainMenu()

# - Pausing -

## Handle UI related input
func _input(event : InputEvent): # [1]
	# - Handling ESC Key -
	if event.is_action_pressed("ui_cancel"):
		if consoleOpen:
			hideConsole()
			return # Get out so the game's pause state doesn't toggle.
		if paused:
			paused = false # If the pause menu is open, pressing ESC always closes it.
			return
		if !isMainMenu && !paused:
			paused = true
	# - Handling Tilde Key -
	if event.is_action_pressed("console"):
		consoleInstance.showConsole()
		consoleOpen = true # Again, keeping track of this seperately.

## This is here to make sure that BOTH the close command and the escape key can close the console.
func hideConsole():
	consoleInstance.hideConsole() # If the console is open, hide the console on any scene
	consoleOpen = false # Keeping track of this seperately.

# Would love to have the ESC console closing code in the module itself, but it's easier to handle here for now. <1814Sun11Feb24>

# - Loading And Control -

# NOTE: Later on, add an exception in clean for the console, so respawns are not required.

## Loads the main menu, and calls any other methods that need to be done.
func loadMainMenu(): # Loads the main menu
	# NOTE: This is also called by the pause menu to quit to title.
	paused = false # This is here to make sure that everything unpauses.
	isMainMenu = true
	# - Clean & Console -
	clean()
	spawnModules()
	# - Load Main Menu -
	mainMenuInstance = mainMenu.instantiate() # instance the main menu, and fill out the variable at the top. # <1332Thu03Aug23>
	add_child(mainMenuInstance) # Add it to the scene
	move_child(mainMenuInstance,0) # Move the mainmenu as the first child [1] <1749Sun11Feb24>
	connectSignals(mainMenuInstance) # Connect signals, this is for the buttons.

## Load a level into the game, and start a map.
func loadLevel(levelName):
	clean()
	spawnModules() # Spawn the console, ready ahead of time for the level objects to be spawned in.
	# - Loading Screen -
	print("[MasterRoot] Loading started for " + levelName)
	# - Load -
	var gameSceneInstance = gameScene.instantiate() # Instantiate the game scene
	add_child(gameSceneInstance) # Add the game scene as a child to the scene <1450Tue01Aug23>
	move_child(gameSceneInstance,0) # Move the gamescene as the first child [1] <1749Sun11Feb24>
	# - Delete Loading Screen -
	
	# - Set States -
	isMainMenu = false
	# - Set Level & Start Game -
	gameSceneInstance.setLevel(currentLevel) # Set the current level.
	gameSceneInstance.startGame(false) # Start the game! Currently, checkpoints are not implented, so we always just start from checkpoints because it's whatever regardless! <1332Fri28Jul23>

# - Cleaning -

## Remove any non-module children from MasterRoot, 'cleaning' the scene.
func clean():
	# - Clean Main Node -
	for main_child in get_children(): # Get all the children
		if main_child != modules: # If this is not the modules node:
			main_child.queue_free() # Remove from queue
	# - Clean Modules -
	#for module_child in modules.get_children():
	#	module_child.queue_free() # Remove from game

# - Console -

## Spawns in the modules, including the console and screenshots, among other modules needed by the system.
func spawnModules():
	# NOTE: Initially, the console was wiped. Now, if a console already exists, do not respawn it. One console should be part of the game at all times.
	# - (Re)spawn Console -
	if consoleInstance == null:
		consoleInstance = console.instantiate() # First, instance the console to the world...
		modules.add_child(consoleInstance) # ...and add it to a child of the modules node.
		connectCommandSignals() # Connect any signals to the STC!C CLI.
		consoleInstance.hideConsole() # Then hide the console!
	# - (Re)spawn Screenshots Module -
	if screenshotsInstance == null:
		screenshotsInstance = screenshots.instantiate() # First, make an instance of the screenshots module.
		modules.add_child(screenshotsInstance)

# - Main Menu Buttons (Signal Connectees) -

## Hides the menu when transitioning the main game
func hideMenu():
	mainMenuInstance.queue_free() # Delete the main menu.
	var alphaPopupInstance = alphaPopup.instantiate() # Instantiate alphaPopup instance
	add_child(alphaPopupInstance) # Add the child to the master root node (this object)
	alphaPopupInstance.find_child("Play_Button").pressed.connect(campaign) # Start campaign 

## Campaign mode, to be implemented fully in the beta
func campaign():
	loadLevel(currentLevel)

## Set the level to play
func setLevel(level : String):
	currentLevel = level
	print("[MasterRoot] Picked level of " + currentLevel)

## Quits the game
func quit():
	print("Quitting Sunny The Cockatiel! Classic!")
	get_tree().quit() # This quits the game

# - Returns -
## Check wether or not input should be sent to the bird
func denyInput() -> bool: # This functions returns if the inputs being made in functions should be denied. Used to fix issues with the console and inputs being made whilst the console is open.
	return paused or consoleOpen # Return true if paused or the console is open.

# = SIGNALS =

# - Pausing Signals -
signal togglePause(isPaused : bool) ## Used for toggling the pause menu.
signal close() # Moved here from the console, so that closing the console using the command also allows input to be accepted again.

## Connect required signals, namely to buttons in this case.
func connectSignals(instance): # Connect signal
	# - Main Menu Buttons -
	# Level Buttons
	# NOTE: These are requied to find fron the instance due to the instance variable changing, meaning the buttons may be null objects if we don't do this.
	var introductionImpasseButton : Button = instance.find_child("IntroductionImpasse_Button") # Find button
	var snowlandSkywayButton : Button = instance.find_child("SnowlandSkyway_Button")
	var introductionImpasseCallable : Callable = setLevel.bind("Introduction_Impasse") # Make a callable with the correct level as the arg
	var snowlandSkywayCallable : Callable = setLevel.bind("Snowland_Skyway")
	introductionImpasseButton.pressed.connect(introductionImpasseCallable) # Now, connect the set level signal 
	snowlandSkywayButton.pressed.connect(snowlandSkywayCallable)
	introductionImpasseButton.pressed.connect(hideMenu) # Then connect the hidemenu method
	snowlandSkywayButton.pressed.connect(hideMenu)
	#TODO: Find a way to store which level is to be used, or add a script to the button (via code preferably?) that connects the signal itself. Should be done for Beta, where Single Level would be an actual mode.
	# Quit Button
	var quitButton = instance.find_child("Quit_Button")
	quitButton.pressed.connect(quit)
	# REMEMBER: Multiple signals to one button is possible. Be aware of multiple signals on one button.

## Called just after the console is spawned, to allow the pause button to close the console.
func connectCommandSignals():
	# - Console Commands -
	consoleInstance.close.connect(hideConsole) ## Allows for closing the console.

# == REFERENCES ==
# [1] [https://forum.godotengine.org/t/is-it-possible-to-rearrange-nodes-through-code/33476] <1749Sun11Feb24>
# [2] [https://forum.godotengine.org/t/how-to-focus-a-lineedit-node-from-script/22226/3] <1808Sun11Feb24>
