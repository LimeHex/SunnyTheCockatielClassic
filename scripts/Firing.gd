class_name Firing extends Node2D

# Firing.gd - Isolates firing to it's own module. Can be attached to plovers that need to fire, if any.
# Written by Jazztache - 1417Sun28Jan24

## Firing.gd is a script that is used on Sunny (and possibly Plover in future or other STC!C-based codebases)
## It enables Sunny to fire in an [i]8-way directional-locked fashion[/i], and includes direction snapping functionality and setting the inital velocity of the screech.

# == VARIABLES ==

# - Bullet Values -

@export var bullet_cooldowntime : float = 2 ## This is BOTH the persistance time for the bullet and the time until you can next fire.
@export var bullet_projectilespeed : float = 500 ## This is the speed a bullet flies through the air. Later on, define this in distance over time as two seperate variables.
@export var firing_cooldown : float = 0.9 ## This is a new variable implemented to make the code more readable and modify the firinjg cooldown later on.

# - State Values -

var isFiring : bool = false ## Check if the user is firing
var canFire : bool = true ## Used to detect when Sunny can fire, connected to a timer signal. <1524Tue11Jul23> 
var headPosition : Vector2 ## Constantly updating pointer to Sunny's head position.
var aim : Vector2 ## Directional vector that respesents aiming. Set by other scripts.

@export var flip_threshold : float = 0.05 ## Deadzone where Sunny will not flip. This isn't relative to Sunny, this is relative to the input vector, which is why this is such a small number.

# - Control Method Values -
# NOTE: Implementation could be a bit better, but since Godot is robust enough to work for the other types of inputs, I'll let this one slide just for now
# Only used to effect firing.

# Objects
# Note about the preload function, that won't work, because Sunny is being instanced into the world.
@export var screech : Resource ## This is the filepath to the bullet object, which is fired from a point specified by [code]firingCast[/code]'s collisions
@onready var root : CharacterBody2D = $"../.." ## The root object, which is the Sunny object that holds everything.
@onready var screecher : AudioStreamPlayer2D = $"../../BodyParts/HeadPivot/HeadContainer/Screecher" ## The specific Screecher point on Sunny
@onready var head : Node2D = $"../../BodyParts/HeadPivot/HeadContainer" ## This will be rotated <1558Tue04Jul23> ## Note: This was updated in order to solve an issue with head rotation animations.
@onready var fireTimer : Timer = $"../../FireTimer" ## The fire timer object, only used on Players.
@onready var firingCast : RayCast2D = $"../../BodyParts/HeadPivot/HeadContainer/FiringCast" ## The Raycast2D Object that is used to enable proper LHTB-defined firing, checking if an object is in the way and spawning it at the colliding object instead.

@onready var rotsca : Node2D = $"../RotSca" ## This is used to signal to the RotSca script to rotate the head.
@onready var inputShow : Label = $"../../InputShow" ## A debugging label used to show inputs, seldom used outside of debugging.
# == FUNCTIONS ==
func _ready():
	connectSignals()

func _process(_delta):
	# - Updaing Values -
	headPosition = head.get_global_position()
	# - Rotate Head -
	rotsca.rotateHead(aim) # Call rotsca to rotate the head.
	# - Flip Sunny -
	if aim.x < -flip_threshold: # Aim is a negative number, if it's on the right.
		rotsca.flipCharacter(1) # Face right if global mouse position is more than headposition
	elif aim.x > flip_threshold: # Aim is a positive number, if it's on the left.
		rotsca.flipCharacter(-1)

# - Firing -
# NOTE: This will be useful if you decide to implement screeching on Plovers for extra difficulty. Composition is a wonderful thing!

## This runs when fire is called, which is called from PlayerInputs, or whatever the enemy equivalent is.
func fire():
	# - Cooldown Check -
	if not canFire:
		return # Timer has not expired yet, do not do anything. <1534Tue11Jul23>
	# Referenced from https://www.youtube.com/watch?v=HycyFNQfqI0 <1603Tue04Jul23>
	var screech_instance = screech.instantiate() # This instances a screech <1559Tue04Jul23>
	#Next three lines apply values. <1619Tue04Jul23>
	# - LHTB-Style Position Adjustment -
	var aimRotation = aim.angle() ## Get the angle of the aim rotation in radians
	var aimReach = -firingCast.get_target_position() # This works, because the x position is used instead of the y.
	var spawnPosition = firingCast.global_position + aimReach.rotated(aimRotation) # Default to the head position, plus however many units long the target position is.
	if firingCast.is_colliding():
		#print("Collision found at " + firingCast.get_collider().get_name() + ", setting spawn position to " + str(firingCast.get_collision_point()) + " instead of " + str(spawnPosition)) # This print statement lists and changes that may happen because of collision.
		spawnPosition = firingCast.get_collision_point() # Get the collision point
	# - Apply Values -
	screech_instance.position = spawnPosition # Spawn position of the screech is based around Sunny's head, either at the tail of the raycast, or at whatever it collided with. <1608Tue04Jul23> <0809Wed28Feb24>
	screech_instance.rotation_degrees = head.global_rotation_degrees # Use Sunny's head in order to set the rotation. NOTE: Be sure to use global rotation, as it accounts for flipping.
	get_tree().get_root().call_deferred("add_child",screech_instance) # Reparent the screech to the root node instead of to Sunny/Snowy
	# - Audio -
	if not screecher.playing: # Check if the audio is playing. <2057Thu06Jul23>
		screecher.play()
	# - Cooldown -
	canFire = false; # Prevent Sunny from firing again...
	fireTimer.start(firing_cooldown) # ...until this timer hits zero, starting from two seconds. <1535Tue11Jul23>

## Set the aim direction, used from other scripts.
func setAim(aimDirection : Vector2):
	aim = aimDirection

# == SIGNALS ==

## Connect signals to (and usually not) from other scripts
func connectSignals():
	fireTimer.timeout.connect(cooldownTimeout) # Connects the Godot-autogenerated signal to the timeout method. <1815Sun28Jan24>
	# NOTE: Remember: node, signal, connect, method.

## A signal function that is called from the cooldown timer, for when the timer runs out. Restores the ability to fire.
func cooldownTimeout(): # IS SIGNAL ON SUNNY/TIMER
	canFire = true; # Re-enable firing.
	pass # Replace with function body.



# == REFERNECS ==
# [1] [https://www.youtube.com/watch?v=4pqfN9OMS70] <1906Sun25Feb24>
