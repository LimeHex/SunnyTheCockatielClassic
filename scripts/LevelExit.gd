# LevelExit.gds
# Written By Jazztache
# 1954Sat29Jul23

class_name LevelExit extends Node2D
## A small script for handling the signals related to ending a level, as well as the Warp Nest that is used for the level exit.

# = VARIABLES =
@onready var masterRoot = get_node("/root/MasterRoot/") ## This is used to ask wether we can use the instawin button. Check this for the console as well if you want cheats or something i guess.
@onready var gameScene = get_node("/root/MasterRoot/GameScene/") ## This is the variable that handles gamescene stuff. It is static in order to save on memory if there is more than one way to exit a level. Usually there isn't though. <1956Sat29Jul23>

# = SIGNALS =
signal entered() ## Fired when Sunny enters the touchbox.

## Usual Godot boilerplate.
func _ready():
	entered.connect(gameScene.levelExited) # Connect the signal

## Insta-win debugging function.
func _input(event):
	if event.is_action_pressed("instawin"): # If you pressed instawin
		# Wait, is this allowed?
		if masterRoot.denyInput():
			return # If input is denied, return.
		entered.emit() # Instantly win, used for testing.

## Called by a signal connected graphically. When Sunny touches the touchbox, entered is emitted.
func _on_level_exit_touchbox_area_entered(_area):
	entered.emit() # Emit the entered signal
	pass # Replace with function body.
