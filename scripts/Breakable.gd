class_name Breakable extends Node2D
## Breakable.gd - A script for breakable tiles, where certain conditions can cause blocks to break.
## Jazztache - 1243Thu25Apr24

# = VARIABLES =
# - Values -
@export var breakOnTouch : bool = true ## Break this tile if a player or enemy touches it.
@export var breakDelay : float = 0.1 ## The time it takes for the object to break.
# - References -
@onready var breakableObject : Node2D = $"../.." ## The object to break.
@onready var interactionArea : Area2D = $"../../InteractionArea" ## The Area2D to use with on-touch breaking.
# - Particles -
# TODO: Implement particles
# = FUNCTIONS =

## Default Godot Boilerplate
func _ready():
	connectSignals()

## Break the object. extra args are needed because the area_shape_exited signal always returns 4 arguments. Yes, you DO NEED 4 args, even if you just don't use them. I tried.
func breakObject(_area_rid,_area,_area_shape_index,_local_shape_index):
	if breakOnTouch:
		spawnVisualEffects() # Spawn any required visual effects...
		breakableObject.queue_free() # ... then break the object

## Spawn any visual effects related to breaking, namely, objects with a pre-set animation.
func spawnVisualEffects():
	pass
# = SIGNALS =

## Connect signals up at runtime.
func connectSignals():
	#TODO: Make a particle effect for entering the breakable.
	interactionArea.area_shape_exited.connect(breakObject) # Break the object if entered.
	pass


func _on_interaction_area_area_shape_exited(_area_rid, _area, _area_shape_index, _local_shape_index):
	spawnVisualEffects()
	pass # Replace with function body.
