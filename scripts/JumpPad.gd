class_name JumpPad extends Node2D
# JumpPad.gd - A script that drives the jump boost on jump pads.
# Jazztache - 1403Thu25Apr24

## The script for the jump pad in Snowy Skyland.

# = VARIABLES =
# - Values -
@export var jumpMultiplier : float = 1.5 ## How much jump boost is applied. Keep it at 2 for now.
# - References -
@onready var interactionArea : Area2D = $"../../InteractionArea" ## The area used to check for if Sunny or enemies are standing in the box.

# = FUNCTIONS =

# - Boilerplate -
func _ready():
	connectSignals()
# - Jump Boost Application -

## Set the jump boost multiplier on object(s) entering the interaction box of the pad, provided it has a Movement.gd script applied to a script node.
func applyJumpBoost(_area_rid,area : Area2D,_area_shape_index,_local_shape_index):
	var movement = getMovementScript(area)
	if movement == null: return
	movement.setJumpBoost(jumpMultiplier) # Send the current jump boost multiplier to the Sunny or enemy that walked over it.

## Remove the jump boost multiplier, on object(s) entering the interaction box of the pad, as long as it has a Movement.gd script in a script node.
func removeJumpBoost(_area_rid,area : Area2D,_area_shape_index,_local_shape_index):
	var movement = getMovementScript(area)
	if movement == null: return
	movement.resetJumpBoost() # Send the current jump boost multiplier to the Sunny or enemy that walked over it.

## Get the movement script node from an area, called from adding and removing.
func getMovementScript(area: Area2D) -> Node2D:
	if area == null:
		return null # This is here to deal with Screeches being an area without a parent.
	var areaParent = area.get_parent() # Get the parent, so we can find the movement script!
	var movement = areaParent.get_node("Scripts/Movement") # Ok, get the movement script!
	return movement # Nulls are handled in the main scripts themselves.
	# This worked the first time :D <1439Thu25Apr24>
# = SIGNALS =

## Connected any necessary signals. Just interaction areas for applying and removing the jump boost.
func connectSignals():
	interactionArea.area_shape_entered.connect(applyJumpBoost)
	interactionArea.area_shape_exited.connect(removeJumpBoost)
