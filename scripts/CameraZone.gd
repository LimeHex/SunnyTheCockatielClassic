# CameraZone.gd
# 1032Tue25Jul23
# Written By Jazztache
class_name CameraZone extends Area2D
## This is a collisable object that triggers the camera to move.

@onready var main = get_node("/root/MasterRoot/GameScene") ## This is where the gamescene is.
var entered = false ## This keeps track of wether this has been entered or not
@export var awardPlayer = true ## This is disabled on the first screen by default.
@export var firstScreen = false ## This is a new variable to set the camera position. <1450Tue01Aug23>

# = VARIABLES =

# = REPORTING =

## When an area is entered, fire this function.
func _on_area_entered(_area):
	main.updateCameraPositionTo(position) # Update the camera position. (Accidentally removed this)
	if !entered: ## If not entered, then let's add screens entered.
		if awardPlayer: # If we're awarding points
			main.incrementClearedScreenCount() # Implementing this as a method instead of a signal is a bit easier. Maybe implement as a signal later? <1120Sun30Jul23>
		entered = true # Regardless, set entered to true.
	pass # Replace with function body.

# = INIT =

func _ready(): # Check if this is the first screen, if it is, snap the camera there.
	if firstScreen:
		main.setCameraPositionTo(position) # Set the camera position immeditely. <1517Tue01Aug23>
