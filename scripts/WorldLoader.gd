class_name WorldLoader extends Node2D
# WorldLoader.gd
# Written By Jazztache
# 1237Fri28Jul23

## WorldLoader loads in the objects that make up the world, including Sunny. Note that Sunny is considered to be part of the game world, she and the level are seperate children of the 'World' node. Checkpoints are also handled here.
# As In:
# GameScene/World/Level
# GameScene/World/Sunny

# = VARIABLES =
# - Spawnable Objects -
@export var playerObject = preload("res://prefabs/characters/Sunny.tscn") ## This is the player object that gets spawned in.
# - Spawning And Checkpoints -
var playerSpawnPosition = Vector2(0,0) ## The spawn position of Sunny, including if she has touched a checkpoint. Note: This could be wrapped in as spawnPOsitions[0] for more terse code. <1936Sat22Jul23>
var defaultWorldSpawnPoint = Vector2(0,0) ## This is the default world spawn. It's used for Sunny's first-time spawn, as well as the 'Retry From Start' pause menu command.
var dwspNodeName = "DefaultWorldSpawnPoint" ## This is the name of the node you are looking for when you are getting the spawn point. Target node is to be a Marker2D. <1347Fri28Jul23>
# - Level List / Spawnable World Objects -
var currentLevel : String ## This is the string that gets put into the loader string when a level is to be loaded.
# The worlds are spawned based on their name, so no need for an internal ID system or internal map system, as just the level's filename will suffice.
# Explanation
# WHEN A MAP IS SELECTED
# 1. This script is told which level they will spawn
# 2. The world is spawned
# 3. Any spawn positions are set up
# 4. Sunny is spawned or respawned
# WHEN THE PLAYER RETRIES
# 5. World And Sunny instance are destroyed and reloaded on loss
# 6. COntinue from step 2.

# NOTE: Any round/UI/Score logic is in the GameMaster, we're just handling the 'physical' game nodes that the player interacts with.

# = PICKING A LEVEL TO SPAWN =

## Called by the GameMaster in order to pick a level by it's ID.
func pickLevel(internalLevelName):
	print("[WorldLoader] Flying off to level " + internalLevelName + "...") # Keep this print statement, useful for catching future level loading bugs.
	currentLevel = internalLevelName # Set the current Level to the what was passed in..
	#TODO: Find a way to do this from a file instead of with a switch statement. This will make your games easier to mod and inject levels into in future.

# = SPAWNING WORLD =

## Called in GameMaster after a few events.
## 1. Retrying triggers this
## 2. Losing and reloading the level triggers this, even if at a checkpoint.
func spawnWorld():
	# Get the required object and instance it into the world!
	var currentSpawnedLevel = load("res://levels/" + currentLevel + ".tscn").instantiate() # Instance the level
	# Spawn in the object
	add_child(currentSpawnedLevel) # Spawns in a level and adds it as a child.
	# Set any variables based on spawn positions for Sunny
	var defaultSpawnPointMarker = currentSpawnedLevel.find_child(dwspNodeName,true) # This is the variable that stores the spawned level's default marker, this is used to get the default spawn position. get_child is called from the level's node itself.
	setDefaultWorldSpawnPoint(defaultSpawnPointMarker.global_position) # Set the default spawn point position based on the Marker2D's position in the scene.
	pass

# = SPAWNING SUNNY =

## Called in the GameMaster after the world is spawned.
func spawnSunny():
	var sunnyObject = playerObject.instantiate() # Create an instance of Sunny in the scene.
	add_child(sunnyObject) # Add that instance of sunny to the scene.
	sunnyObject.global_position = playerSpawnPosition # Code for setting Sunny's position is here, uses playerSpawnPosition
 
# = CHECKPOINTS & SPAWN LOCATIONS =

## Used to set the spawn position for the player, accounting for all scenarios.
## This is called in a few scenarios.
## 1. When the map is loaded for the first time or changes.
## 2. When a checkpoint is reached.
## 3. When the player chooses to restart from beginning. <1245Fri28Jul23>
func setPlayerSpawnPosition(pos):
	playerSpawnPosition = pos
	#print("MapLoader.gd has set the player spawn to " + str(pos))

## Called when a level is loaded, so that it can be referenced again incase the player retries the map from the beginning.
func setDefaultWorldSpawnPoint(pos):
	defaultWorldSpawnPoint = pos
	#print("MapLoader.gd has set the world spawn to " + str(pos))

## Called when a player restarts from the beginning of a level, OR after a default spawn point is set.
func resetPlayerSpawnPositionToDefault():
	playerSpawnPosition = defaultWorldSpawnPoint # Reset the player's position back to default.

# = CLEANING SCENE =

## Calls queue_free on all children. Called when a map is retried, or in preperation for it being loaded.
func cleanScene():
	pass

# = FULL WRAPPER =

## Does everything in one hit. Really useful for full-resets or loading the level initially.
func init(levelId,resetPlayerPos):
	cleanScene() # Clean the scene's previous objects...
	pickLevel(levelId) # ...Pick a level....
	spawnWorld() #...Spawn in the world...
	if (resetPlayerPos): # ... Do you want to reset the player position? ...
		resetPlayerSpawnPositionToDefault() # ... If so, then reset the player position. Spawn in the world first so we know what the default is. (See 2 lines above) <1353Fri28Jul23>
	spawnSunny() #...Spawn Sunny!

# = RETURNS =

## Returns the current level
func getCurrentLevel() -> String: ## This is used to return the string <1110Sun30Jul23>
	return currentLevel
