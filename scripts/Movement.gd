class_name Movement extends Node2D
# Movement.gd
# Written by Jazztache
# 1409Sun28Jan24

## This class governs how both Players and Enemies are allowed to move. Both of these use this script as part of their composition.

# == VARIABLES ==

# - Input -
# NOTE: Input can be either from an enemy or a player. On player, it's controls, on enemies it's AI.
var horizontalInput = 0
var verticalInput = 0

# - Objects -
@onready var root = $"../.." ## Root object that holds the main player. MUST be a CharacterBody2D
@onready var floor_check = $"../../FloorCheck" ## A RayCast2D that checks the ground, much nicer than using is_on_floor.

# Movement Variables
# NOTE: These were referenced from https://gist.github.com/sjvnnings/5f02d2f2fc417f3804e967daa73cccfd
# The constants for jumping were derived from the above script, which uses a projectile motion physics calc to derive fall speed, jump speed and gravity.

@export var acceleration_factor = 1.0 ## In future, this will be used to implement a linear acceleration to time function. Or alternatively as a time to get to max speed int he same vain as the variables below.
@export var move_speed = 5.0 ## Basic movement speed for Sunny and Plovers.

#TODO: Look into the acceleration formula for a car, or an object with intertia, and then use that. Keep it small as the movement should still feel arcade-y, but make sure the acceleration goes from 0.5 to 1 over the course of lets say, 0.5s 2150Wed05Jul23
#@export var base_speed # 2155Wed05Jul23
#@export var top_speed # 2155Wed05Jul23
#@export var move_time_to_top_speed # 2156Wed05Jul23

# - Input Buffering -
# Originally in PlayerControls, make more sense as part of movement, which I figured out during noclip implementation.
# NOTE: Due to a bug with how the signals are set up, inputs tend to get eaten with jumps....
# ...To rectify this, the just pressed gives 0.05s of time where jump is 'turbo pressed'.

var jumpInputTime = 0 ## Resets everytime the player jumps, and then ticks up.
var jumpInputTimeThreashold = 0.05 ## Gives a 50ms jump buffer, allowing for more input leniancy. It feels better and your intentions are better realised.
# - Processing -
@export var disabled = false ## This variable is reserved for scripts that need to be disabled. Implemented manually.
# Later on, calculate Sunny's speed, given base_speed, top_speed, and move_time_to_top_speed <2157Wed05Jul23> <1558Fri21Jul23>
# - Physics Variables: Jump And Fall -
@export var jump_height : float = 100 ## Physics variable for jump height
@export var jump_time_to_peak : float = 1 ## Physics variable for the time it gets for Sunny to reach the peak of the jump
@export var jump_time_to_descent : float = 1 ## Time for Sunny to fall to the ground.

@onready var jump_velocity : float = ((2.0 * jump_height) / jump_time_to_peak) * - 1.0 ## Calculated on start for how 'hard' the Player/Enemy jumps.
@onready var jump_gravity : float = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak)) * - 1.0 ## Calculated on start for how fast falling is.
@onready var fall_gravity : float = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent)) * - 1.0
# - Hard Drop + Coyote Time -
# NOTE: timeSinceGrounded is the same in order to prevent a bug where SUnny insta-jumps when retrying stages from the pause menu.
var coyoteDisableJumpDelay: float = 0.25 ## One second before jumping midair is disabled.
var hardDropMultiplier : float = 4 ## Double gravity during hard drop
var isHardDropping : bool = false ## Used to check if the user is inputting the hard drop, and isn't grounded
var timeSinceGrounded : float = coyoteDisableJumpDelay ## This is used to measure the time since leaving a platform.
# - Jump Boost -
var jumpMultiplier : float = 1 ## What jump multiplier to add on. Use with jump pads in Snowland Skyway.
# == FUNCTIONS ==

# - Get Input -

## A signal from the PlayerControls script module is connected to here.
func inputs(horizontal,vertical):
	horizontalInput = horizontal
	verticalInput = vertical

# - Repeating -

## Process Physics
func _physics_process(delta):
	if disabled: return
	move_entity(delta)

# - Physics Calculation / Frame Step =

## This is run on every frame. Generic enough to be used for both Players and Enemies. func_process is called in children scripts.
func move_entity(delta):
	#NOTE: Inputting hard drop CANCELS coyote
	# - Horizontal Velocity -
	root.velocity.x = horizontalInput * move_speed #* acceleration_factor
	# Later on, figure out how to implement delta into the horizontal movement.
	# - Vertical Velocity -
	jumpInputTime += delta # Always increment the value that holds how long since the last jump. This is used for the input buffer on jumping.
	isHardDropping = verticalInput == -1 # Check if the entity is using the hard drop.
	root.velocity.y += get_gravity() * delta
	if floor_check.is_colliding(): 
		timeSinceGrounded = 0 # Reset the time since the entitly has left a grounded state
	if verticalInput == 1 and timeSinceGrounded < coyoteDisableJumpDelay and jumpInputTime > jumpInputTimeThreashold: # If the player is 1. inputting a jump, 2. you're within coyote time, and 3. It's been more time since the last jump than jump input time threashold, jump.
		jump()
		timeSinceGrounded += coyoteDisableJumpDelay # Increase the time since grounded such that you can longer re-jump whilst in midair.
		jumpInputTime = 0 # Reset the input time since the last jump. This causes the next if statement to fire.
		# NOTE: Notice that this is a just-pressed and not a regular pressed. ...
		# ...This is to prevent people from automatically perch-vaulting.
		# Perch-vaulting is a mechanic where you can jump without being solidly on the ground, so long as Sunny's feet collide with a platform. Yes, even whilst she is still vertically moving.
		# It only triggers on the way down however, just so it's balanced & non-trivial but still fun to use and execute well.
	# - Move Character -
	root.move_and_slide()
	# - Timer -
	timeSinceGrounded += delta # Increment the amount of time since last left a platform. This gets reset to zero next tick if grounded.
	#print("[Movement] Horizontal: " + str(horizontalInput) + " | Vertical: " + str(verticalInput))

## Allows the player, enemy or boss to jump.
func jump():
	root.velocity.y = jump_velocity * jumpMultiplier # Set the Y velocity to the jump velocity

# - Stage Hazards: Jump Pads -

## Reset the jump boost multiplier value back to 1.
func resetJumpBoost():
	jumpMultiplier = 1

## Set the jump boost multiplier value to an arbitrary value.
func setJumpBoost(value):
	jumpMultiplier = value

# - Returns -

## Get the gravity of Sunny given the hard drop value
func get_gravity() -> float:
	if root.velocity.y > 0.0 && !isHardDropping: # If you're rising, and not hard dropping...
		return jump_gravity # ...Use the jump gravity
	else: # Otherwise, 
		return (fall_gravity * get_harddrop()) # Use teh hard drop.

## This is the function for the harddrop multiplier. Remember: set it to one to disable it.
func get_harddrop() -> float:
	#if velocity.y < 0: print(str(isHardDropping) + " with velocity of " + str(velocity.y))
	return hardDropMultiplier if isHardDropping else 1.0 # Return the floating-point value of the hard drop multiplier.
# NOTE: SOMEHOW You cannot detect if Sunny is falling or on a flat surface. Sunny only outputs numbers other than zero if rising during a jump. Very sad.
