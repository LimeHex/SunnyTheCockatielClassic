class_name PlayerControls extends Node2D

# PlayerControls.gd - Script that isolates player controls, be they keyboard + mouse or controller. NOT for Enemies. Sunny-specific!
# Jazztache -1414Sun28Jan24

## Dedicate script for player controls. Handles keyboard and mouse controls, and drives the Movement (or NoClip ;) ) scripts.

# == VARIABLES ==

# - Basic Values -

# These store the very core essential input values that the player can create.
var horizontal = 0
var vertical = 0
var isFiring = false ## Wether or not Sunny is firing.

# - Jump Holding -
# This is like the input buffering system in Sunny (which was originally here) but only to make sure that held space presses don't bhop in regular movement mode.
var jumpHeldThreshold = 0.05
var jumpHeldTime = 0 ## The current time the jump button has been held for.
# - Objects -

# These objects are for the other scripts. These scripts (as you can probably tell by the paths) are all siblings of the PlayerControls script.

@onready var movement : Node2D = $"../Movement" ## Movement module, passing input to this script!
@onready var firing : Node2D = $"../Firing" ## Firing, passing mouse clicks and the firing trigger to this script.
@onready var health : Node2D = $"../Health" ## Health, for damage and hits and such.
@onready var noclip : Node2D = $"../NoClip" ## Used for the noclipping module, mutually exclusive with Movement.

@onready var root : CharacterBody2D = $"../.." ## The root note, the Sunny node.
@onready var head : Node2D = $"../../BodyParts/HeadPivot" ## The pivot point for the head, used as reference for aiming inputs when mouse input is being used. <2004Sun25Feb24>
@onready var collisionHull = $"../../CollisionHull" ## The collision hull for Sunny. It is turned off when noclip is on.

# - Processing

@export var disabled = false ## This variable is reserved for scripts that need to be disabled. Implemented manually.
var aim = Vector2.RIGHT ## The current direction the player is aiming. Passed to the firing script.
var aimtype : String = "Unknown" ## The type of aiming based on the input method being currently used. TODO: Make this an enumeration?

# - Debug -

@onready var console : CanvasLayer ## Set in the connect signals function, this saves where the console is! This is done at runtime.
@onready var master = get_node("/root/MasterRoot/") ## Master root node reference
@onready var inputShow : Label = $"../../InputShow" ## A debugging label for showing the inputs.

var isNoClipEnabled : bool = false ## Used to toggle noclip mode!
var strself : String = "[b]" + "Sunny" + "[/b]:" ## Used for outputs!


# == FUNCTIONS ==

# - Boilerplate -

## The bird is ready to go!!!!
func _ready():
	connectSignals() # Connect the signals <1805Sun28Jan24>
	setNoClip(false)

## Per-tick processing for handling input.
func _process(delta):
	if disabled: return
	if master.denyInput(): return # Something says we cannot process inputs! This solves the issue of moving/killbinding whilst the console is open.
	process_player_inputs(delta) # Every frame, process the player's inputs
	firing.setAim(getAim())

# - Input Processing -

## The main method for processing the player's inputs. If there are any control or physics issues, this might be a good place to start looking if there's trouble.
func process_player_inputs(delta):
	# - Horizontal -
	horizontal = 0.0 # Reset the player's horizontal inputs. This is so buttons aren't 'held' down after the player stops pressing them.
	if Input.is_action_pressed("move_left"):
		horizontal -= 1.0 # -1 is left
	if Input.is_action_pressed("move_right"):
		horizontal += 1.0 # +1 is right
	# - Vertical -
	# Instead of using ternary operators, different if statements were used to handle three states of jump, crouch and neither. This proved handy when jump-input buffering was introduced as well. <2052Sat29Jul23> & <1741Mon29Jan24>
	var canAcceptUpward = (jumpHeldTime < jumpHeldThreshold || isNoClipEnabled) # You can accept upward inputs if noClip is enabled and you are not holding Jump.
	if Input.is_action_pressed("move_jump"):
		jumpHeldTime += delta # Make sure to set the holding jump button!
		if canAcceptUpward: # If it can also accept upward inputs...
			vertical = 1.0 # ...set the vertical input to one!
		else:
			vertical = 0.0
		# ...Originally: This is true so long as it's less than 50 ms since the jump button was last pressed. See movement for where this moved to.
	else: # Alright, so the player isn't pressing the jump button.
		jumpHeldTime = 0 # Ok, we are not holding jump.
		if Input.is_action_pressed("move_crouch"):
			vertical = -1.0 # Ah! They're pressing the 'crouch' button. In the air this is hard-drop.
		else: 
			vertical = 0.0 # ... If neither input is pressed, do nothing!
	#inputShow.text = ("Jump Held Time: " + str(jumpHeldTime) + "| No Clip Enabled: " + str(isNoClipEnabled) + " | Can Jump: " + str(canAcceptUpward))
	# TODO: Convert the difference between mouse cursor pos and head pos into a Vector2 and use a normalised, 8-angle snapped version of that instead.
	# - Check For Firing -
	isFiring = Input.is_action_pressed("fire") # Store the result of is firing as a variable, this is used for holding down the key. <1636Tue04Jul23>
	# - Firing Check -
	if isFiring: # Remember, this gets set above with the is action pressed statement.
		fire.emit() # Call the fire method if the fire input is held.
	if Input.is_action_just_pressed("killbind"):
		kill.emit() # Le funni killbind!!!!!!!!!!!!!!
	move.emit(horizontal,vertical) # Use the move signal to emit horizontal and vertical info to the actual movement script.

# Remember kids, underengineer and overcomment! This is probably the one script where I can tutorialise Sunny's core coding and scripting the most.

## Used to get the aim, prioritises controller inputs.
func getAim() -> Vector2:
	var controllerAim = Input.get_vector("aim_right","aim_left","aim_down","aim_up") # [1] # NOTE: Right and left are reversed in order to make the control stick and mouse output the same values.
	if controllerAim != Vector2.ZERO: # If the controller aim is not zero...
		aim = controllerAim # ...return controller aim!
		aimtype = "Controller"
	elif Input.get_last_mouse_velocity() != Vector2.ZERO: # If the last mouse velocity is zero... (NOTE: check the method docs, it might lag the game later on, rework to use an inputevent?)
		# For mice...
		var mouseAim = head.global_position - get_global_mouse_position() # Get the head's global position in the world, and take away the global mouse position.
		aim = mouseAim # Finally, return the mouse aim.
		aimtype = "Mouse"
	aim = aim.normalized() # Normalise the numbers.
	inputShow.text = aimtype + ": " + str(aim)
	return aim # Return aim

# - NoClip -
# Useful for debugging levels and getting level designs tested and working.

## Used to toggle the noclip state, Allows you to toggle cthe noclip state on and off.
func noclipCommand(option : String = ""):
	# NOTE: This is a major rework of the noclip command, added to make it have parity with the debug and cheats commands.
	# - Check Cheats -
	if !console.getCheatStateToObject():
		console.send("You can't noclip if you don't have cheats on! Cheats disable scoring. Use `cheats on` to enable cheats.")
		return
	# - Set NoClip -
	var old_isNoClipEnabled : bool = isNoClipEnabled
	match option:
				"on","true","1":
					setNoClip(true)
					console.send("Noclip has been [b]enabled[/b]! Go nuts! High scores have been disabled for this run... Was: " + str(old_isNoClipEnabled))
				"off","false","0":
					setNoClip(false)
					console.send("Noclip has been [b]disabled[/b]. Was: " + str(old_isNoClipEnabled))
				"toggle":
					setNoClip(!isNoClipEnabled)
					console.send("Toggled noclip to [b]" + str(isNoClipEnabled) + "[/b]. Was: " + str(old_isNoClipEnabled))
				"show":
					console.send("Current value of isNoClipEnabled: [b]" + str(isNoClipEnabled))
				"": # Toggle, AND show full usage
					setNoClip(!isNoClipEnabled)
					console.send("Toggled noclip to [b]" + str(isNoClipEnabled) + "[/b]. Was: " + str(old_isNoClipEnabled) + ". Usage: `cheats [on/off/toggle/show]`. Current value: [b]" + str(isNoClipEnabled))
				_:
					console.send("Usage: `noclip [on/off/toggle/show]`. Current value: [b]" + str(isNoClipEnabled))

## Used when someone EXPLICITLY wants a state. Will be useful when scripting comes for some other non-STC!C game.
func setNoClip(noClipState):
	isNoClipEnabled = noClipState # Set it to false.
	# - Collision -
	collisionHull.disabled = noClipState # Hide the collision hull
	# - Toggle Processing - # [2]
	movement.disabled = noClipState # Turn movement to the opposite of the noclip state
	noclip.disabled = !noClipState # Turn noclip to the noclip state
	# - Signals -
	if noClipState:
		move.disconnect(movement.inputs) # Disconnect regular movement...
		move.connect(noclip.inputs) # ...and connect noclip movement
	else:
		if not move.is_connected(movement.inputs): # If we are not connected to the movement inputs...
			move.disconnect(noclip.inputs) # ...Disconnect noclip movement...
			move.connect(movement.inputs) # ...and connect regular movement

# - Teleport -

## Teleport function, only should be used if cheats are enabled.
func teleportCommand(xPos : String = "", yPos : String = ""):
	# - Check Cheats -
	if !console.getCheatStateToObject():
		console.send("You can't teleport if you don't have cheats on! Cheats disable scoring. Use `cheats on` to enable cheats.")
		return
	# - Teleport -
	if xPos == "" and yPos == "":
		console.send("Usage: `tp [float:x] [float:y]` or alternatively replace the floating-point numbers with '~' for current x/y position. Remember: for Y axis, negative numbers send you skyward.")
	else:
		var currentPos = root.position
		var xPosAsNum = currentPos.x if xPos == "~" else xPos as float # Convert x position to number, including accounting for ~ as current x.
		var yPosAsNum = currentPos.y if yPos == "~" else yPos as float # Convert y position to number, including accounting for ~ as current y.
		var tpPos = Vector2(xPosAsNum,yPosAsNum) # Make the vector # [3]
		root.set_position(tpPos) # Set the player's position
		console.send("[PlayerControls] " + strself + " was teleported from X" + str(currentPos.x) + "Y" + str(currentPos.y)  + " to X" + str(xPosAsNum) + "Y" + str(yPosAsNum)) # Tell the user

# - Console Utils -

# == SIGNALS ==

signal fire() ## The signal to trigger firing for the firing component.
signal kill() ## The signal to trigger killbinding for the health component. <1454Sun28Jan24>
signal move(horizontal,vertical) ## The signal to send movement inputs. <1457Sun28Jan24> Also works with noclip. <1333Tue13Feb24>

signal send(text) ## Used to output to the console

# - Testing -

## Bing! If you bing the console, it bongs you back. Used for testing when signals get tempermental.
func bong():
	console.send("[b][i]Bong!") # Send it to the console! Why was I doing this with print_rich?

# - Connect Signals -

## Connect any required signals. This includes the fire/kill/move signals and stuff like noclip/tp/bing signals for console terminal tomfoolery
func connectSignals():
	# - Companion Composed Scripts -
	# Here is where we communicate with our fellow scripts!
	fire.connect(firing.fire) # Take the fire signal, and connect it to firing's fire function.
	kill.connect(health.kill) # Take the kill signal, and connect it to health's kill function.
	move.connect(movement.inputs) # NOTE: This is connected via GUI. If something happens, try to see if this works.
	# - Find Console -
	console = get_tree().get_first_node_in_group("console") # Get the console. This is the most code-efficient way to do this without descending the tree. If there are signal connection problems, I have verified that it potentially isn't here.
	print_rich("[PlayerControls] " + strself + " found console of " + str(console.name) + " at " + str(console.get_path()))
	# - Console Connections -
	send.connect(console.send) # Allow this object to send text to the terminal 
	console.noclip.connect(noclipCommand) # Allow the console to toggle noclip
	console.tp.connect(teleportCommand) # Allow for teleportation commands to be sent for debugging
	console.bing.connect(bong) # Bing bong! It's the test command!

# == REFERENCES ==
# [1] [https://docs.godotengine.org/en/stable/tutorials/scripting/groups.html] <1614Sun11Feb24>
# [2] [https://forum.godotengine.org/t/how-to-disable-enable-a-node/22387] <1341Tue13Feb24>
# [3] [https://forum.godotengine.org/t/what-does-invalid-call-nonexistent-vector2-constructor-mean/14092] <1338Fri16Feb24>
