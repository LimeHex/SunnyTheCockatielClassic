class_name EnemyBrain extends Node

# EnemyBrain.gd - The brain used for enemy tracking.
# Jazztache - 1511Sun28Jan24

## A function that is used instead of p;layer input to drive the enemies. Since we are using composition, Plovers use EnemyBrain as part of their composition in the same way Player uses PlayerControl.

# TODO: Make a resource for tweaking values. This will be useful if the STC!C codebase gets used to make BIRBOUT! v2.0.

# == VARIABLES ==

# - Hard Constants -

const XMOVEMENT_IF_ABOVE_TARGET = 500 ## To to this many units left or right of Sunny if you are above her, allowing Plovers to walk off platforms to path to Sunny.
@export var XMOVEMENT_TARGET_CLOSENESS_THRESHOLD = 200 ## The amount of units from the target to cease movement. Should be such that collision boxes intersect. <1507Mon17Jul23>
const YOFFSET_TO_START_SIDEPATHING = 100 ## How far above the plover needs to be to start navigating XOFFSET_ON_BELOW_TARGET to the side. <1511Sat15Jul23>
const YOFFSET_TO_JUMP = 200 ## This is how far above Sunny has to be from a plover for it to jump.

# - Working Values -
var objectToTrack ## This is the variable for tracking Sunny as an enemy.
var horizontalInput = 0
var verticalInput = 0

# - Objects -
@onready var root = $"../.." # MUST be a CharacterBody2D
@onready var movement = $"../Movement"
@onready var detectionBox = $"../../DetectionBox"

@onready var rotsca = $"../RotSca" ## Used for interfacing with the rotating and scaling.
# == FUNCTIONS ==

# - On Ready -
func _ready():
	connectSignals()

# = - Main Loop =

func _physics_process(delta):
	process_enemy_inputs(delta) # This is for enemies. ALl in one it first processes their brain ("where is birb" bits), then second process their legs (moving bits)
	rotateScale() # used to call rotsca to make sure the head rotation and scaling is good.

# - Thinking -

func enemySpotted(area): # IS SIGNAL ON ENEMY
	var areaParent = area.get_parent() # Get the parent
	objectToTrack = areaParent # Track that bird. <1615Fri21Jul23>
	# See the process_enemy_inputs functions after this.

# This is for the AI. Note: This is the kind of thing that gets split off into it's own Enemy.gd script, which inherits from this one. <1742Sat15Jul23>
func process_enemy_inputs(_delta):
	# - Pre-Checks -
	if objectToTrack != null: # No onbect to track? Leave. Otherwise, continue with the AI train.
		# - Vertical Movement -
		# Is Sunny above the Plover?
		verticalInput = 1 if root.position.y - objectToTrack.position.y > YOFFSET_TO_JUMP else 0 # Is sunny's position YOFFSET_TO_JUMP units higher than this Plover's position?...
		# ...Simple! Jump! <1518Mon17Jul23>
		# Is Sunny below the Plover?
		var pacingDistance = XMOVEMENT_TARGET_CLOSENESS_THRESHOLD # This is how far Plovers pace back and forth to follow or Damage Sunny. Changes to be a much bigger constant when Sunny is below, in order to add situational pathing and unstick the plover from higher perches. <1530Mon17Jul23>
		#NOTE: Add a ramming animation for the plover when touch damage is applied successfully. This makes it look less wierd and static.
		if objectToTrack.position.y - YOFFSET_TO_START_SIDEPATHING > root.position.y: # Remember, in 2D, the number is BIGGER as you go DOWN, this is the opposite of Unity Engine. Rare Godot L.
			pacingDistance = XMOVEMENT_IF_ABOVE_TARGET # This increases the pacing distance to unstick Plovers from higher ledges. Also has the benefit of making the pathing more dynamic in vertical platforming situations. <1531Mon17Jul23>
		else:
			pacingDistance = XMOVEMENT_TARGET_CLOSENESS_THRESHOLD
		# - Horizontal Movement -
		if !abs(objectToTrack.position.x - root.position.x) < pacingDistance: # Check the distance, and then check if it's NOT inside the threshold, in order to decide X movement.
			horizontalInput = 1 if objectToTrack.position.x > root.position.x else -1
		# - Input Cleaning -
		horizontalInput = clamp(horizontalInput,-1,1) # Prevent the value from going below a magnitude of 1, keeps the inputs similar to the player.
		# - Debugging -
		#stateShow.text = "!: " + str(horizontalInput) + "," + str(pacingDistance) + "," + str(verticalInput) # This shows alert, pacing distance and direction, and the jump input as one set of values. <1553Mon17Jul23>
	move.emit(horizontalInput,verticalInput) # Call back the original EntityCharacter.gd script.
	#TODO: Factor deltatime into this to make it consistent between framerates, then fix the speed factor to compensate.

func rotateScale():
	# Copied from the firing node, needed new variables, which is why it is here.
	if objectToTrack == null:
		return # Nothing to track, nothing to do.
	# - Rotate Head -
	var trackPosition = objectToTrack.position
	var lookVector = root.position - objectToTrack.position # Set a look vector for the plover. Updated for the sake of working with the new aiming system for controller + keyboard support. <1944Sun25Feb24>
	rotsca.rotateHead(lookVector) # Call rotsca to rotate the head.
	# - Flip Plover -
	if trackPosition.x > root.position.x: 
		rotsca.flipCharacter(1) # Face right if track position is more than origin
	elif trackPosition.x < root.position.x:
		rotsca.flipCharacter(-1)
# == SIGNALS ==

signal move(horizontal,vertical) # Yep, some things just have to be copied. Not that much with composition though. <1522Sun28Jan24>

func connectSignals():
	move.connect(movement.inputs)
	detectionBox.area_entered.connect(enemySpotted)
