class_name ProjectileMotion extends RigidBody2D

# Jazztache - (Unknown Date, Wasn't Documented)

## The script used to decide how a projectile moved.

@export var speed = 2.5 ## Determines the speed of the projectile.
@export var time = 1 ## Determines how long the bullet is to be alive, usually inherited from Sunny herself, and set. Alternatively, you can have a specific value to reference somewhere in the code.

var distance ## alculated from speed and time, and is used for debugging. Time is to be set in accordance with Sunny's screech cooldown.
var elapsedTime = 0.0 ## This is used to calculate how much time has been elapsed. <1248Mon10Jul23>

var stopped = false ## Wether or not the screech is stopped

@onready var visuals = $ScreechSprite ## This is the sprite for the screech, used when colourising things.

var aimsnap = true ## This decides wether there is aimsnapping or not.
var debugaimcolourisation = false ## Do you want rainbow colours? Great! Good for debugging purposes! # Set back to normal. <1918Sat29Jul23>

var basicColour = Color(0,190,255) ## Decides the colour of the screech!

# TODO: `aimsnap [on/off/toggle/show]` Imagine the shenanigans! Lock this behind cheats.

## Screech at the ready!
func _ready():
	# = AIMSNAP =
	var degreesRotation = rad_to_deg(rotation) # get the rotation in degrees.
	degreesRotation = getRange(degreesRotation) # Get the degrees of rotation using the method, and put it back into the variable.
	if debugaimcolourisation: # If we want debug colours...
		colouriseScreechDebug(degreesRotation) #...call the debug colours method.
	if aimsnap: # If we want aimsnap....
		rotation = deg_to_rad(degreesRotation) # ...Convert back to radians, and use that to set rotation

## Return a number snapped to an integer, based on which range of values is being used.
func getRange(originalRotation) -> int:
	# Use a series of if statements to decide which value to return.
	# Use the range value that has been decided on to return out the value.
	# Make sure we're staying withing the same amount of rotations.
	# = 360 CLAMPING =
	var roundedRotation = int(round(originalRotation)) # Round the original rotation to an integer
	while roundedRotation < 0: # If the rounded rotation is a negative number...
		roundedRotation += 360 # ... Add one full rotation to make it positive but on the same angle.
	roundedRotation %= 360 # Use modulo so it doesn't go above or below 360.
	# = AIMSNAP =
	if roundedRotation < 22.5 && roundedRotation >= 337.5:
		return 0
	if roundedRotation < 67.5 && roundedRotation >= 22.5:
		return 45
	if roundedRotation < 112.5 && roundedRotation >= 67.5:
		return 90
	if roundedRotation < 157.5 && roundedRotation >= 112.5:
		return 135
	if roundedRotation < 202.5 && roundedRotation >= 157.5:
		return 180
	if roundedRotation < 247.5 && roundedRotation >= 202.5:
		return 225
	if roundedRotation < 292.5 && roundedRotation >= 247.5:
		return 270
	if roundedRotation < 337.5 && roundedRotation >= 292.5:
		return 315
	return 0 #  Default to 0, straight ahead.

## Debugging function colouring the screech a specific colour at specific angles.
func colouriseScreechDebug(aimAngle):
	# TODO: `debug colouriseScreechDebug [on/off/toggle/show]`
	if !debugaimcolourisation: # Check if aim colourisation is on
		return # If it's off return, otherwise it's rainbow screech time! Leave this in as a cheatcode! Yay! <1335Mon10Jul23>
	var debugColor = Color(0,0,0)
	match aimAngle: # Use a switch statement to select a colour to colourise the screech. This is based on the aimsnap calculation, even if aimsnap is turned off.
		0: # Note: Please find a way to use RGB 0 to 255 colours in the constructors, don't use like 0 to 1 decimal.
			debugColor = Color(1,1,0)
		45:
			debugColor = Color(1,0.5333,0)
		90:
			debugColor = Color(1,0,0)
		135:
			debugColor = Color(1,0.6274,1)
		180:
			debugColor = Color(6.4392,0.2,1)
		255:
			debugColor = Color.BLUE # This also doesn't display correctly. <1501Mon10Jul23>
			#debugColor = Color(0.1098,0,0.8431) # For some reason, this doesn't display properly, instead displaying as black. Very sad. <1500Mon10Jul23>
		270:
			debugColor = Color(0,0.5490,0.5490)
		315:
			debugColor = Color(0,0.7843,0.2431)
	visuals.modulate = debugColor # Change the color to the picked one.
	# Using a switch statement, match the aimsnapped values to their colours! 

## Projectile movement
func _process(delta):
	# TODO: Make this a bit more modular with more functions. Give it some breathin' room!
	if stopped:
		pass
	# = MOVE IN STEP =
	position += Vector2(speed,0).rotated(rotation)
	# = COLLISION ACTIONS =
	
	# = TIMER CHECK =
	elapsedTime += delta # Increment the timer.
	if elapsedTime >= time: # If the elapsed time is more than the time, destry the object. <1249Mon10Jul23>
		stopped = true  # This is a workaround until I can figure out how to force deallocated objects to not call func process on themselves.
		free() # This deallocates the screech object.
	pass

# [https://godotlearn.com/godot-3-1-how-to-destroy-object-node/] <1255Mon10Jul23>
