# GameMaster.gd
# 1832Sat22Jul23
# Written by Jazztache
class_name GameMaster extends Node2D
## This script is in charge of the game flow, and win/loss conditions

# NOTE: Move camera code to it's own script if you need the extra cleanliness
# NOTE: move UI code to it's own script if you need the extra cleanliness

# TODO: Make Loss UI, Make Win UI, Make Losing, Make Winning

# = VARIABLES AND OBJECTS =
# - Main Objects -
@onready var worldLoader = $World ## This is for communicating with the game's worldloader.
# - Camera -
@onready var camera = $Camera2D ## This is the game's camera. If null, no camera movement is to be done.
var cameraTrackObject ## Which object to track. Usually Sunny. Used for both camera zones and also the birb-cam. Zooms in on the plover that killed you (if implemented)
var cameraMoveTime = 0 ## This is used for how far along the track the camera is.
var cameraFrom = Vector2(0,10) ## This is where the camera moves from
var cameraTo = Vector2(0,10) ## This is where the camera moves to
var cameraShouldMove = false ## This is if the camera should be moving.
var cameraMaxMoveTime = 0.25 ## This is used to limit when the camera should move, as a timeout.
@export var cameraLerpCurveFactor = 4 ## This is used for multiplying the camera speed. MATHS: This is meant to complete in 30 frames, or 0.5s, since the animation uses an exponential curve.
# - Levels -
@export var loadLevel : String ## This is used to define to the WorldLoader which level should be loaded, regardless of game modes and the like, be it Level Select or Campaign.
# - Win / Loss -
enum States{Playing, Won, Lost} ## The states the game can be in.
static var state = States.Playing ## The current state of the game.
var attempts = 0 ## The amount of completed attempts this round. Add one to this for the #1, #2, e.t.c. of this attempt.
# - UI  Main Objects -
@onready var mainUI = $Camera2D/MainUI ## The main control node for the UI, which holds everything. Can be disabled in noUi mode.
@onready var wonUI =  $Camera2D/MainUI/RoundWonUI # This is the UI for the winning screen
@onready var lostUI = $Camera2D/MainUI/RoundLostUI # This is the UI for the losing screen
@onready var startUI = $Camera2D/MainUI/RoundStartUI # This is the UI for when a round starts, telling the player which level they are on, and which attemp they are on this session
@onready var ingameUI = $Camera2D/MainUI/InGameUI
var noUi = false ## Used for testing, lets debuggers (and maybe players) turn off the UI if they wish.
# - UI Subobjects -
@onready var scoreUI = $Camera2D/MainUI/InGameUI/Score # This is for the score.
@onready var healthUI = $Camera2D/MainUI/InGameUI/Health ## This is displaying the health. Also, really like the custom BBcode you can do with this.
# - UI Round Text -
@onready var winDescription = $Camera2D/MainUI/RoundWonUI/WinPanel/WinDescription ## This is the text for when you win!
@onready var loseDescription = $Camera2D/MainUI/RoundLostUI/LosePanel/LoseDescription ## This is the text for when you win!
# - Gameplay Variables -
var score = 0 # The agme's score. Every seed is 10 points, every plover kill should be 100 points.
var clearedScreens = 0 # The amount of screens cleared, used on the UI. <1114Sun30Jul23>
var killedPlovers = 0 # The amount of killed plovers
#Array checkPointPositions = new Array() # This is an array of checkpoint positions.
var checkPointReached = 0 ## This is an int that changes based on which checkpoint you are at. The spawn point at the start is considered checkpoint 0. Each checkpoint is to have a unique ID attached to it for that map.

# == FUNCTIONS =
# - Boilerplate -

func _process(delta):
	moveCamera(delta)

func _ready():
	pass # NOTE: The start game thing was moved to the master root, in order for the level to be correctly chosen.

# - Startup -
## Set the level. Called from the MasterRoot before startgame.
func setLevel(level : String):
	loadLevel = level # Set the level

# This is called when a new round is to be started. 
# 1. This is automatically called when the game logic starts up for the first time on the scene being instanced.
# 2. This is done when retrying a map from a win, or retrying a map from a loss. These are different paths for code readability and cleaner more readable structure. <1856Sat22Jul23>
func startGame(fromStartOfMap):
	# Reset The Score
	score = 0 # Reset the score
	clearedScreens = 0 # reset the cleared screens as well, cannot forget about that! <1114Sun30Jul23>
	killedPlovers = 0 # Reset the amount of plovers killed
	# Reset UI Elements
	scoreUI.text = "[center]0[/center]"
	healthUI.text = "[b]5[/b]hp"
	# Reset World Objects
	# [https://www.reddit.com/r/godot/comments/9qmjfj/remove_all_children/] Bro had to credit the reddit <1933Sat29Jul23>
	for i in worldLoader.get_children(): # Get all the children
		worldLoader.remove_child(i) # Remove the child
		i.queue_free() # Free that child from it's chains of existance :) <1934Sat29Jul23>
	# Tell the WorldLoader variables
	if (fromStartOfMap):
		worldLoader.resetPlayerSpawnPositionToDefault() # This resets this to the default world position, if the player is playing from the start of the map. Either loading it initially, or if it's a forced from-start retry
	# Do UI Stuff
	hideLossUI()
	hideWinUI()
	showGameUI()
	# Prompt the WorldLoader to load in the world.
	worldLoader.init(loadLevel,true) # This initialises the level. This isn't always from start, as that's done before.
	# Note, there will have to be a method for loading a picked level from the disk, so it persists between scenes.

func spawnPositionReset():
	checkPointReached = 0 ## Clear checkpoint prtogress

# - Camera - 

func getCamera(): # This is used to get the camera for position tracking
	pass

func updateCameraPositionTo(to): # Update the camera position based on the current position and a new one.
	if camera == null: return # Can't do anything, so prevent this from running.
	# Assign the variables
	cameraMoveTime = 0
	cameraFrom = camera.position ## Use the camera's current position.
	cameraTo = to
	cameraShouldMove = true
#TODO: Make it so that the camera can snap into the correct place when the lerpTime is done.

func setCameraPositionTo(to): # Set the camera position immeditely. Used by any initial screen. <1519Tue01Aug23>
	if camera == null: return # Return if camera is null.
	camera.position = to # Set the camera position

func updateCameraPosition(from,to): ## Update the camera's position from a set point to a new point.
	if camera == null: return # Can't do anything, so prevent this from running.
	# Assign the variables
	cameraMoveTime = 0
	cameraFrom = from
	cameraTo = to
	cameraShouldMove = true
#TODO: Make it so that the camera can snap into the correct place when the lerpTime is done.

func moveCamera(delta_move): ## This moves the camera using the assigned camera variables. This is used to interpolate the camera from one positon to the other.
	if !cameraShouldMove: return ## No camera movement
	# From here interpolate the camera
	# Use a position is 1 divided by frames since last moving. (y = 1/x)
	### FIX: Make this an exponential curve and not a hyberbola. maybe something like y=-2^{0.1x}+5
	cameraMoveTime += delta_move
	var interpEquationTimePosition = (cameraLerpCurveFactor*cameraMoveTime) ## This is where we are on the equation
	#print("The camera is at " + str(interpEquationTimePosition))
	# Find the requested midpoint of the camera
	var fullDistance = cameraTo.distance_to(cameraFrom) ## Get the distance
	var vectorDifference = cameraTo - cameraFrom ## Get the vectors to and from, 
	var offset = fullDistance * interpEquationTimePosition * vectorDifference.normalized() ## Set a magnitude of where the camera should be, based on how far through the equation we are.
	# The calculated position is the base from the camera, plus the offset as a multiplier for the magnitude, which is multiplied with the vector difference. <1214Tue25Jul23>
	var calculatedPosition = cameraFrom + offset # This is calculating the offset
	#print(str(calculatedPosition) + " is |" + str(cameraFrom) + " + (" + str(fullDistance) + " * " + str(interpEquationTimePosition) + " * " + str(vectorDifference) + ")|")
	# Physically move the camera
	camera.position = calculatedPosition ## Move the camera to the positional offset.
	# Check if done or the frames has expired
	if (cameraMoveTime > cameraMaxMoveTime):
		cameraShouldMove = false
		# If so, just snap the camera at the end (should be unnoticable), and disable camera movement, so this doesn't keep triggering.
		camera.position = cameraTo

# - Win -
func winState():
	state = States.Won
	showWinUI()
	endRound() # Generic round end code
	# Pass onto the UI that the game has been won.
	var cleanedMapName = str(worldLoader.getCurrentLevel()).replace("_"," ") # This is the map name, but the underscores get changed to spaces. This is a good and cheap way of always having the filename and the levelname correspond to each other.
	winDescription.text = cleanedMapName + " has been cleared!"

# - Loss -
func loseState():
	state = States.Lost
	showLossUI() # Show the losing UI
	endRound() # Do generic code for ending the round.
	# Pass onto the UI that the game has been lost.
	var plural = "s" if clearedScreens != 1 else "" # Make sure that the plural form of screens is used
	loseDescription.text = "You cleared " + str(clearedScreens) + " screen" + plural + " and KO'd " + str(killedPlovers) + " enemies." # Show the death description.

# - Scoring

## Add a point
func incrementPloverKillCount():
	killedPlovers += 1 # Add one to the plover kill count
	score += 100 # The obvious. # Every plover is now worth 100 kills as of <1114Sun30Jul23>
	scoreUI.text = "[center]" + str(score) + "[/center]" # Update the UI. <1706Sat29Jul23>

# Add one to the amount of screens and also increase the score by 1000 points!
func incrementClearedScreenCount():
	clearedScreens += 1
	score += 1000
	scoreUI.text = "[center]" + str(score) + "[/center]"

# = END =
func endRound():
	# Hide the game UI <2005Sat29Jul23>
	hideGameUI()
	# Save score to scoreboard

# This is for ending the game and contains saving functionality and anything that happens after a game.
# Nothing about STARTING a new game should be here. UI Signals should manage that instead.

# - UI Functionality -
# - UI: Show -
## Show the UI. Namely health icons, e.t.c.
func showUI():
	mainUI.visible = true

## Show the round start UI. Level intro, attempts, e.t.c.
func showRoundStartUI():
	startUI.visible = true

## Show the round won UI. 
## Similar to BIBROUT!1. Uses the same campaign and respawn text BIRBOUT! does.
func showWinUI():
	wonUI.visible = true

## Show the round lost UI. Similar to BIBROUT!1
func showLossUI():
	lostUI.visible = true

## Show The in-game UI
func showGameUI():
	ingameUI.visible = true

# - UI: Hide -

## Hide the UI. Namely health icons, e.t.c.
func hideUI():
	mainUI.visible = false

## Hide the round start UI. Level intro, attempts, e.t.c.
func hideRoundStartUI():
	startUI.visible = false

## Hide the round won UI. 
## Similar to BIBROUT!1. Uses the same campaign and respawn text BIRBOUT! does.
func hideWinUI():
	wonUI.visible = false

## Hide the round lost UI. Similar to BIBROUT!1
func hideLossUI():
	lostUI.visible = false

## Hide The in-game UI
func hideGameUI():
	ingameUI.visible = false

# - Button Functionality -

func retryFromCheckpoint():
	startGame(false) # Start the game without removing any checkpoint progress.

func retryFromStart():
	startGame(true) # Restart the game from the start
# GameMasterScript.gd
# Camera Zones
# Win Condition
# Lose Condition
# Basic UI



# = SIGNAL CONNECTIONS =

## Connect signals to other nodes!
func connectSignals():
	pass

## Called when a plover dies. Signal is connected via code at runtime via each plover instance. 
func ploverDied():
	incrementPloverKillCount()

func playerDied():
	loseState()

func playerHit(health):
	healthUI.text = "[b]" + str(health) + "[/b]hp" # Make the player's health UI update.

# Exit the level; win.
func levelExited():
	winState()

# This is for the UI button when you click restart.
func _on_retry_button_pressed():
	retryFromCheckpoint()
	pass # Replace with function body.

func _on_restart_button_pressed():
	retryFromStart() # Retry the level from the start. Useful in future. <2016Sat29Jul23>
	pass # Replace with function body.
# == REFERENCES ==
# Note, some references may have been lost, as I forgor....
# [1] [https://odysee.com/@ChrisTutorials:b/how-to-make-a-pause-game-menu-pause-the:1?t=37] <1241Thu08Feb24>
