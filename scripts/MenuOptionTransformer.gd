class_name MenuOptionTransformer extends Control

# MenuOptionTransformer.gd - A script that detects when an option is changed or loaded, and then updates it's objects accordingly. 
# Jazztache - 1301Sun25Feb24

## Responsible for detecting when an in-game option is loaded or changed, updating objects as it needs to. 

# == VARIABLES ==

@export var changableObjects : Array[Node] ## The objects that update when the options change.
var console

# == FUNCTIONS ==

## Ready... only for connecting signals.
func _ready():
	connectSignals()

# - Make Changes -

## Change the scale of the object
func changeScale(newSize : float):
	for changableObject in changableObjects:
		var previousScale = changableObject.get_scale() # Get the previous scale for the output
		var scaleVector = Vector2(newSize,newSize) # Make a size vector
		changableObject.set_scale(scaleVector) # Set the size of the object
		console.send("Changed scale of " + changableObject.name + " from [b]" + str(previousScale.x) + "[/b] to [b]" + str(newSize) + "[/b].") # Report change to console

## Change the opacity of an object
func changeOpacity(newOpacity : float):
	for changableObject in changableObjects:
		var previousColour = changableObject.get_self_modulate() # Get the colour
		var newColour = previousColour # Get the new colour as a duplicate
		newColour.a = newOpacity # Set the opacity
		changableObject.set_self_modulate(newColour) # Set the self modulate
		console.send("Changed opacity of " + changableObject.name + " from [b]" + str(previousColour.a) + "[/b] to [b]" + str(newOpacity) + "[/b].") # Report change to console

# - Receive Changes -

## The menu command that is sent with signals
func menu(subcommand,ternvalue):
	match subcommand: # Get the subcommand
		"opacity":
			changeOpacity(float(ternvalue)) # and change opacity
		"scale":
			changeScale(float(ternvalue)) # and change scale
		# In each subcommand, be sure to specify this is a floating point number, or whichever type is EXPLICITLY defined in each method.

# == SIGNALS ==
## Connect the main option co-ordinator (be that the master or something else) to this script
func connectSignals():
	# - Console Connections -
	console = get_tree().get_first_node_in_group("console")
	console.menu.connect(menu)


# == REFERENCES ==
# [1] [https://www.reddit.com/r/godot/comments/7nf42t/need_help_with_creating_array_of_nodes/] <1336Sun25Feb24>
# [2] [https://github.com/godotengine/godot/issues/62916] <1346Sun25Feb24>
