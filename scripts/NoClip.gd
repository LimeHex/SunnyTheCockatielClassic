class_name NoClip extends Node2D

# NoClip.gd - The Noclip node that is used to enable noclip on Sunny. Mutually exclusive with the movement node.
# Jazztache - 1310Tue13Feb24

## When cheats are on, the fun begins! Alternate replacement script for Sunny's movement when the [code]noclip[/code] function is on.

# NOTE: later on lock this behind the `cheats` command, which is yet to be implemented.

# = VARIABLES =
# - Input Processing -
var horizontalInput = 0 ## Any horizontal inputs being passed in
var verticalInput = 0 ## Any virtical inputs being passed in
# - Movement Variables -
@export var noClip_speed : float = 500 ## Movement speed when noclipping
# - Objects -
@onready var root : CharacterBody2D = $"../.." ## The root player node
# - Processing =
@export var disabled : bool = true ## This variable is reserved for scripts that need to be disabled. Implemented manually.
# = FUNCTIONS =

# input and repeating are taken from Movement as the base.

# - Get Input -
## A signal from the Movement script module is connected to here.
func inputs(horizontal,vertical):
	horizontalInput = horizontal
	verticalInput = vertical

# - Repeating -

## Process physics
func _physics_process(delta):
	if disabled: return;
	move_entity(delta)

# - Processing -

## Move entity under the effects of no-clip
func move_entity(_delta):
	root.velocity.x = horizontalInput * noClip_speed
	root.velocity.y = -verticalInput * noClip_speed
	# - Move Character -
	root.move_and_slide()
	# TODO: Make delta factor into movement.

# == REFERENCES ==
