class_name Health extends Node2D

# Health.gd - isolate health, death and hitbox stuff
# Jazztache - 1439Sun28Jan24

## Health, damage, invulnerability, hitpoints, and death for Enemies and Players alike!

# == VARIABLES ==

# Values

@export var isPlayer = false ## Wether or not the character is a player, influences what kinds of signals are emitted.
@export var health = 5 ## Health of the player/plover, usually 1 for plovers until later in the levels.
@export var invulnerabilityTime = 2 ## How long this object is invulnerable.

#NOTE: I can't use CharacterBody2D or Timer here, because you cannot call methods on them directly. You have to use Node2D and then call from there.
@onready var root = $"../.." ## The CharacterBody2D at the top of the tree
@onready var invulnTimer = $"../../InvunTimer" ## The invuln (i-frames) timer object, only used on Players.
@onready var hurtbox = $"../../Hurtbox" ## Sunny's hurtbox

var canBeHit = true ## This is connected to a timer signal for invulnerabiltiy. <1523Tue11Jul23>

# == FUNCTIONS ==
# - Ready -
## Y'know, the usual.
func _ready():
	connectSignals() # Connect the death signal.

# - Timer Expiration -

## This is used for when the i-frames timer runs out
func invulnTimeout(): # IS SIGNAL ON SUNNY/TIMER
	canBeHit = true; # Re-enable being hit
	pass # Replace with function body.

# - Hit/Hurt Boxes and Collisions -

## Check if the hurtbox is colliding with another hitbox
func hurtboxCheck(area):
	# - Hit Detection -
	if isPlayer:
		if canBeHit: # Can you take damage?
			# NOTE: This works because the layers and layer masks are Properly set. Sunny of layer 2 can mask (interact with) player 3 (Enemies) and vice versa. See the 'Hurtbox' object on each one. <1301Mon17Jul23>
			damage()
			
	else:
		# Plover stuff ported from the enemy script. Simpler on purpose.
		if area.name == "ScreechCollision": # Check wether it's a screech.
			health -= 1 # Decrement health
	# Dying
	if health <= 0:
		dead.emit()
		root.queue_free()
	# TODO: Add in flashing for when your invuln timer it about to run out.

## Function for taking damage
func damage(damage : int = 1):
	health -= 1 # Decrement health...
	canBeHit = false; # ...Turn on invulnerability...
	invulnTimer.start(2) # ...And start a 2 second timer.
	hit.emit(health) # Emit the signal that you got hit, report the current health amount as well. Signals need to emit their args between the brackets. <1911Sat29Jul23>

# - Kill -

## The function triggered when killed
func kill():
	# Health stuff
	health = 0
	hit.emit(0)
	# Death stuff
	dead.emit()
	root.queue_free()

# = SIGNALS =

signal dead() ## Emitted when killed
signal hit() ## Emitted when hit

## Connect signals, in this case, hurtbox and invulnTimer default Godot signals, and some player/enemy specific signals
func connectSignals():
	# Connect up a signal when spawned
	# - Signals From Other Nodes -
	hurtbox.area_entered.connect(hurtboxCheck)
	invulnTimer.timeout.connect(invulnTimeout)
	# - Hit And Death Gamemaster-
	var gameMaster = get_node("/root/MasterRoot/GameScene") # Get the root scene, this supplies the method we are going to connect our signal to. <1743Sat29Jul23>
	if isPlayer:
		dead.connect(gameMaster.playerDied) # get our signal, and then connect it to the other scripts method.
		hit.connect(gameMaster.playerHit) # Send the player hit signal. <1902Sat29Jul23>
	else:
		dead.connect(gameMaster.ploverDied) # get our signal, and then connect it to the other scripts method.
# TODO: Add a blue on grey bar for the firing cooldown, and a white on grey bar for the invuln cooldown. Both have to be displayed at once. <1604Tue11Jul23>
