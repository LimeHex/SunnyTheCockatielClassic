## STC!CScreenshotsMain - A dedicated module for taking screenshots.
## Jazztache - 1157Thu14Mar24

# NOTE: In future, when LimeHexTechBase is in production, make sure the dedicated input manager can see this.

extends Node

# == VARIABLES ==

var saveDir: String = "user://Screenshots" ## The save location for the screenshots.

# == METHODS ==

# - Boilerplate -

func _unhandled_input(event):
	if event.is_action_pressed("screenshot"): # If screenshot is pressed, screenshot.
		takeScreenshot()

func _ready():
	initialiseFolder()

# - Actions & Calculation -

## Initialises the folder for screenshots.
func initialiseFolder():
	var dir = DirAccess.open("user://") # Open the user folder.
	if dir == null: # Check if screenshots can be taken. DirAcess returns null if it fails.
		print("ERROR: Directory was not defined, and reverted to null. Cannot take screenshot! Report to Jazz!")
	dir.make_dir("Screenshots") # Make the screenshots directory.

## Take the screenshot when the user decides to do so.
func takeScreenshot(): # [1] [3]
	await RenderingServer.frame_post_draw # Wait for the frame to finish rendering.
	var viewport: Viewport = get_viewport() # Get the viewport and store it in the viewport variable for later.
	var texture: Texture2D = viewport.get_texture() # Get the texture of the viewport.
	var image: Image = texture.get_image() # Get the image of texture from the viewport, as an image.
	var formattedDate: String = getDateFormatted() # Get the formatted date
	var savePath: String = saveDir + "/STCC_" + formattedDate # Get the final save path ass as one variable.
	var globalSavePath: String = ProjectSettings.globalize_path(savePath)
	image.save_png(savePath) # Finally, save the image. [4]
	print("Saved screenshot to \"" + globalSavePath + "\".") # Tell the user. NOTE: Make this appear as a console notification..

## Get the formatted date.
func getDateFormatted() -> String:
	# - Vars -
	var dateTimeDict = Time.get_datetime_dict_from_system() # Get the datetime from the system.
	# Next, we split everything up into ymd hms and such.
	var year: String = str(dateTimeDict.year)
	var month: String = str(dateTimeDict.month)
	var day: String = str(dateTimeDict.day)
	var hour: String = str(dateTimeDict.hour)
	var minute: String = str(dateTimeDict.minute)
	var second: String = str(dateTimeDict.second)
	# - Formatting -
	# Make sure to add in the zeros.
	# NOTE: If someone reviewing/autiting the code in the Codeberg repo can find a better way of doing this, and can epxlain it, easy MR.
	if year.length() < 2:
		year = "0" + year
	if month.length() < 2:
		month = "0" + month
	if day.length() < 2:
		day = "0" + day
	if hour.length() < 2:
		hour = "0" + hour
	if minute.length() < 2:
		minute = "0" + minute
	if second.length() < 2:
		second = "0" + second
	# Finally, return the correct datetimestamp. Used in the filename of the screenshots.
	return year + "-" + month + "-" + day + "_" + hour + "-" + minute + "-" + second

# == REFERENCES ==
# [1] [https://www.youtube.com/watch?v=lKt0uWHncf0] <1158Thu14Mar24>
# [2] [https://forum.godotengine.org/t/how-to-get-current-system-time/33695] <1207Thu14Mar24>
# [3] [https://www.youtube.com/watch?v=YhqVLW7eQWg] <1244Thu14Mar24>
# [4] {https://docs.godotengine.org/en/stable/tutorials/io/data_paths.html] <1252Thu14Mar24>
