class_name OptionsMenu extends Control
## OptionsMenu.gd - Used to control all of the special UI in the options menu.
## Jazztache - 0949Wed28Feb24

## OptionsMenu is part of the STC!COptionsMenu module, in pre-preperation for how it will work in LHTB
## This manages all of the visual and UI stuff that the Options Menu uses, and allows for:
## - Setting focus
## - Scrolling the sidebar when certain buttons or inputs are pressed
## - Managing buttons and thier calls to the [code]OptionsManager.gd[/code] script, which is in the same module.

## TODO: Wire this up to OptionsManager when that is done.
## TODO: Make it more obvious that you have the headers selected. Either make it italic, small caps (find a clean way to modularise the script) or just enable the back button again somehow. Maybe stylise it with a transparent polka dot/diagonal dot grid pattern

#region == VARIABLES ==


# - Category Buttons -
@onready var videoButton : Button = $Sidebar/Video ## The video category button, which sets focus to the [code]videoHeader[/code] object.
@onready var audioButton : Button = $Sidebar/Audio ## The audio category button, which sets focus to the [code]audioHeader[/code] object.
@onready var uiButton : Button = $Sidebar/UI ## The UI category button, which sets focus to the [code]uiHeader[/code] object.
@onready var oskButton : Button = $Sidebar/OSK ## The OSK category button, which sets focus to the [code]oskHeader[/code] object.
@onready var setButton : Button = $Sidebar/Set ## The set button, which focuses [code]saveAndExitButton[/code]
# - Set Buttons -
@onready var saveAndExitButton : Button = $SetButtons/SaveAndExit ## The set button for saving the settings and exiting the dialouge window
@onready var revertChangesButton : Button = $SetButtons/RevertChanges ## The set button for reverting any changes settings
@onready var resetToDefaultButton : Button = $SetButtons/ResetToDefault ## The set button for resetting any settings to default.
@onready var discardChangesButton : Button = $SetButtons/DiscardChanges ## The set button for discarding changes and exiting the dialouge window.
# - Category Focus Targets -
@onready var videoHeader : Button = $ScrollContainer/OptionsContainer/VideoHeader ## The video header button, used to get focus from the video side button.
@onready var audioHeader : Button = $ScrollContainer/OptionsContainer/AudioHeader ## The audio header button, used to get focus from the audio side button.
@onready var uiHeader : Button = $ScrollContainer/OptionsContainer/UIHeader ## The UI header button, used to get focus from the UI side button.
@onready var oskHeader : Button = $ScrollContainer/OptionsContainer/OSKHeader ## The On Screen Keyboard header button, used to get focus from the On Screen Keyboard side button.
# - Every Dropdown, Slider and Tickbox Ever -
# TODO: There HAS to be a less YandereDev-ass way of doing this, maybe using children? Considering having this be explicitly names using unique vars will be more helpful for defaulting and stuff, I think that keeping this as is is a good idea.
@onready var resolution_selector : OptionButton = $ScrollContainer/OptionsContainer/Resolution/ResolutionSelector ## A dropdown to select the resolution.
@onready var aa_selector : OptionButton = $"ScrollContainer/OptionsContainer/Anti-Aliasing/AASelector" ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var frame_cap_selector : OptionButton = $ScrollContainer/OptionsContainer/FrameCap/FrameCapSelector ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var fullscreen_toggle : CheckBox = $ScrollContainer/OptionsContainer/Fullscreen/FullscreenToggle ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var cage_opacity_slider : HSlider = $ScrollContainer/OptionsContainer/CageOpacity/CageOpacitySlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var background_opacity_slider : HSlider = $ScrollContainer/OptionsContainer/BackgroundOpacity/BackgroundOpacitySlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var transparent_screenshots_toggle : CheckBox = $ScrollContainer/OptionsContainer/TransparentScreenshots/TransparentScreenshotsToggle ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var colourblind_mode_toggle : CheckBox = $ScrollContainer/OptionsContainer/ColourblindMode/ColourblindModeToggle ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var music_volume_slider : HSlider = $ScrollContainer/OptionsContainer/MusicVolume/MusicVolumeSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var sound_effects_volume_slider : HSlider = $ScrollContainer/OptionsContainer/SoundEffectsVolume/SoundEffectsVolumeSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var voice_volume_slider : HSlider = $ScrollContainer/OptionsContainer/VoiceVolume/VoiceVolumeSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var diagetic_sound_volume_slider : HSlider = $ScrollContainer/OptionsContainer/DiageticSoundVolume/DiageticSoundVolumeSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var main_hud_scale_slider : HSlider = $ScrollContainer/OptionsContainer/MainHUDScale/MainHUDScaleSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var main_hud_opacity_slider : HSlider = $ScrollContainer/OptionsContainer/MainHUDOpacity/MainHUDOpacitySlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var main_crosshair_scale_slider : HSlider = $ScrollContainer/OptionsContainer/MainCrosshairScale/MainCrosshairScaleSlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var main_crosshair_opacity_slider : HSlider = $ScrollContainer/OptionsContainer/MainCrosshairOpacity/MainCrosshairOpacitySlider ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var show_fps_toggle = $ScrollContainer/OptionsContainer/ShowFPS/ShowFPSToggle ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var preferred_osk_selector : OptionButton = $ScrollContainer/OptionsContainer/PreferredOSK/PreferredOSKSelector ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
@onready var always_use_osk_toggle : CheckBox = $ScrollContainer/OptionsContainer/AlwaysUseOSK/AlwaysUseOSKToggle ## One of the many dropdowns, sliders, or toggles that I refuse to document nor rename.
# - Categories -
enum Category{Video, Audio, UI, OSK} ## The categories that correspond to the headers on the options menu. Used for pagination.
var currentCategory : Category = Category.Video ## Teh current category that the user has scrolled to, which is tracked to make sure pagination feels right and jumping between categories using PgUp/PgDn and LB/RB feels good and cohesive.
# - Singleton -
@onready var optionsManager : Node = get_node("/root/OptionsManagerSingleton") ## Reference to the Autoload Singleton for the options menu.
# - Cached Defaults -
@onready var defaultValues : Dictionary = optionsManager.getDefaults() ## A copy of the defaults from the OptionsManager that is cached to save on referring back to the options manager.
#endregion
#region == FUNCTIONS ==
# - Boilerplate -
func _ready():
	connectSignals()
	videoButton.grab_focus()
	revertOptions() # Wait why's this here? Well! It's because reverting the options always makes it revert the sliders back to the config, which is why it's also perfect for the initial setup.

func _input(event):
	if event.is_action_pressed("ui_page_down"):
		paginate.call(1)
	if event.is_action_pressed("ui_page_up"):
		paginate.call(-1)

# - Change Focus + Paging -
## Generic lambda function focus function callable, used to be a convienient bindable way for grabbing focus.
var focus : Callable = func(anyControl : Control):
	anyControl.grab_focus()
	print("Focusing " + str(anyControl))

## Incrementally change which page is being selected. All values are scroll direction, not actual page number
func paginate(scroll : int):
	# - Decide the correct category -
	@warning_ignore("int_as_enum_without_cast")
	var nextChosenPage : Category = currentCategory + scroll # Scroll the page using the int number (This works!?!?)
	if nextChosenPage <= -1: # If it's minus 1 or less...
		@warning_ignore("int_as_enum_without_cast")
		nextChosenPage = nextChosenPage + Category.size() #... run it around!
	@warning_ignore("int_as_enum_without_cast")
	nextChosenPage = nextChosenPage % Category.size() # Clamp it to the correct size.
	# - Focus the correct header -
	match nextChosenPage:
		Category.Video:
			focus.call(videoHeader)
		Category.Audio:
			focus.call(audioHeader)
		Category.UI:
			focus.call(uiHeader)
		Category.OSK:
			focus.call(oskHeader)
	print("Set category to " + str(currentCategory))

## Set the category. Done as you roll over headers in the options menu, and used for keeping track of scrolling for pagination.
var setCategory: Callable = func(category : Category):
	currentCategory = category

# - Set Buttons -

## Call the [code]saveOptions()[/code] function, then call
func saveAndExit():
	saveOptions()
	close()

## The function to revert changes, namely, it asks to close the dialog
func revertChanges():
	revertOptions()

## Reset the sliders to the default position.
## Load the default options from the cached default dictionary, and apply them to the sliders, resetting the sliders to the default position
## Reverting still works, and can still load from the [code]config.cfg[/code], as nothing is overwritten or saved to the config file.
func resetToDefault():
	# - VIDEO -
	resolution_selector.select(0) # For the option buttons, just set it to 0, as that's traditionally the default anyways.
	aa_selector.select(0)
	frame_cap_selector.select(0)
	# - AUDIO -
	music_volume_slider.value = defaultValues["musicVolume"]
	sound_effects_volume_slider.value = defaultValues["soundEffectsVolume"]
	voice_volume_slider.value = defaultValues["voiceVolume"]
	diagetic_sound_volume_slider.value = defaultValues["diageticSoundVolume"]

## Discard changes, closing the dialog without saving, and then reverting any changes to the config afterwards.
func discardChanges():
	askClose()
	if !visible:
		revertOptions() # If the options menu isn't visible, silently reset the slides to match the unchanged config. 
# - Actions -
## Calls the OptionsManager to save the current settings from the values on the sliders, to [code]options.cfg[/code].
## Take the values from the sliders, pack it into a resource file, and send it off to the Manager to process it.
func saveOptions():
	optionsManager.saveConfig()

## Calls the OptionsManager to load the [code]options.cfg[/code] into memory and then replace the sliders, tickboxes and dropdowns's values to the ones last saved.
func revertOptions():
	var reversionValues : ConfigFile = optionsManager.getConfigFromDisk() # Get the config from the options manager, because we are going to revert everything at once. Uses the version currently on disk, because the sliders actively change the main config.
	# - VIDEO -
	# TODO: Find a way to do this that doesn't require a loop!
	var resolutionFromConfig : String = reversionValues.get_value("VIDEO","resolution",defaultValues["resolution"]) # First, get the resolution from the config using the reversion values, thsi gives us a string to compare.
	for i in resolution_selector.item_count: # For each item in the resolution selector...
		if resolution_selector.get_item_text(i) == resolutionFromConfig: # .. Check if the item text is the same as the resolution from the config. The string has to match.
			resolution_selector.select(i) # If it matches, then we have our selection
			break # Break out of the loop.
	var aaFromConfig : String = reversionValues.get_value("VIDEO","antiAliasing",defaultValues["antiAliasing"]) # First, get the resolution from the config using the reversion values, thsi gives us a string to compare.
	for i in aa_selector.item_count: # For each item in the resolution selector...
		if aa_selector.get_item_text(i) == aaFromConfig: # .. Check if the item text is the same as the resolution from the config. The string has to match.
			aa_selector.select(i) # If it matches, then we have our selection
			break # Break out of the loop.
	var frameCapFromConfig : String = str(reversionValues.get_value("VIDEO","framecap",defaultValues["framecap"])) # First, get the resolution from the config using the reversion values, thsi gives us a string to compare.
	for i in frame_cap_selector.item_count: # For each item in the resolution selector...
		if frame_cap_selector.get_item_text(i) == frameCapFromConfig: # .. Check if the item text is the same as the resolution from the config. The string has to match.
			frame_cap_selector.select(i) # If it matches, then we have our selection
			break # Break out of the loop.
	# - AUDIO -
	music_volume_slider.value = reversionValues.get_value("AUDIO","musicVolume",defaultValues["musicVolume"])
	sound_effects_volume_slider.value = reversionValues.get_value("AUDIO","soundEffectsVolume",defaultValues["musicVolume"])
	voice_volume_slider.value = reversionValues.get_value("AUDIO","voiceVolume",defaultValues["voiceVolume"])
	diagetic_sound_volume_slider.value = reversionValues.get_value("AUDIO","diageticSoundVolume",defaultValues["diageticSoundVolume"])

## Ask to close the options menu.
## This is used by Discard Changes when there are unsaved changes.
func askClose():
	pass

## Close the options menu.
## This is done as part of Save And Apply, and on confirmation during an [code]askClose()[/code] call.
func close():
	set_visible(false)
	get_tree().quit() # WARNING: TEMPORARY
#endregion
#region == SIGNALS ==
## Connect any signals that need to be connected. Namely buttons.
func connectSignals():
	# - Connect Sidebar Buttons to Headers -
	# Note: I had to learn what a lambda function is in Godot engine using the Callable docs. <1109Wed28Feb24>
	videoButton.pressed.connect(focus.bind(videoHeader))
	audioButton.pressed.connect(focus.bind(audioHeader))
	uiButton.pressed.connect(focus.bind(uiHeader))
	oskButton.pressed.connect(focus.bind(oskHeader))
	# - Connect Headers To First Item In Category -
	videoHeader.pressed.connect(focus.bind(resolution_selector))
	audioHeader.pressed.connect(focus.bind(music_volume_slider))
	uiHeader.pressed.connect(focus.bind(main_hud_scale_slider))
	oskHeader.pressed.connect(focus.bind(preferred_osk_selector))
	# - Find Category When Hovered On Header -
	videoHeader.focus_entered.connect(setCategory.bind(Category.Video))
	audioHeader.focus_entered.connect(setCategory.bind(Category.Audio))
	uiHeader.focus_entered.connect(setCategory.bind(Category.UI))
	oskHeader.focus_entered.connect(setCategory.bind(Category.OSK))
	# - Slider/Dropdown/Value Functionality -
	# -- VIDEO --
	# First, make a callable with the respective values and passing in the object the value is sourced from
	# Then, use it to connect the signal to the newly created callable.
	var resolutionCall : Callable = optionsManager.setValue.bind("VIDEO","resolution",resolution_selector)
	resolution_selector.item_selected.connect(resolutionCall) # Resolution, with a lot of nested methods just to get the id, the text, and then set values for a bind call in a signal. :IIIII
	var antiAliasingCall : Callable = optionsManager.setValue.bind("VIDEO","antiAliasing",aa_selector)
	aa_selector.item_selected.connect(antiAliasingCall)
	var frameCapCall : Callable = optionsManager.setValue.bind("VIDEO","framecap",frame_cap_selector)
	frame_cap_selector.item_selected.connect(frameCapCall)
	# -- AUDIO --
	var musicVolumeCall : Callable = optionsManager.setValue.bind("AUDIO","musicVolume", music_volume_slider)
	music_volume_slider.value_changed.connect(musicVolumeCall)
	var soundEffectsCall : Callable = optionsManager.setValue.bind("AUDIO","soundEffectsVolume", sound_effects_volume_slider)
	sound_effects_volume_slider.value_changed.connect(soundEffectsCall)
	var voiceVolumeCall : Callable = optionsManager.setValue.bind("AUDIO","voiceVolume", voice_volume_slider)
	voice_volume_slider.value_changed.connect(voiceVolumeCall)
	var diageticVolumeCall : Callable = optionsManager.setValue.bind("AUDIO","diageticSoundVolume", diagetic_sound_volume_slider)
	diagetic_sound_volume_slider.value_changed.connect(diageticVolumeCall)
	# -- UI --
	#TBD
	# -- OSK --
	#TBD
	# - Bottom Buttons -
	saveAndExitButton.pressed.connect(saveAndExit) # Save And Exit
	revertChangesButton.pressed.connect(revertChanges) # Revert Changes
	resetToDefaultButton.pressed.connect(resetToDefault) # Reset To Default

# == REFERENCES ==
# [1] [https://godottutorials.com/courses/introduction-to-gdscript/godot-tutorials-gdscript-08/] <1613Fri26Apr24>
