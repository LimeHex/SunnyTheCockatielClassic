class_name OptionsManager extends Node
## OptionsManager.gd - Part of the STC!COptions module that manages configuration files, and applys them to the options menu and scenes.
## Jazztache - 1134Fri26Apr24
#region = VARIABLES =
# - Values -
var configFileLocation : String = "user://config.cfg"
var defaultValues : Dictionary = { ## Stored data for the default values.
	"resolution": "1080p", ## Targeted Screen Resolution
	"antiAliasing": "8x MSAA",
	"framecap": 0,
	"fullscreen": false,
	"cageOpacity": 0.8,
	"backgroundOpacity":1,
	"transparentScreenshots":false,
	"colourblindMode":false,
	"musicVolume":0.8,
	"soundEffectsVolume":0.8,
	"voiceVolume":0.8,
	"diageticSoundVolume":0.8,
	"mainHudScale":1,
	"mainHudOpacity":1,
	"crosshairScale":1,
	"crosshairOpacity":0.5,
	"showFPS":false,
	"preferredOSK":"None",
	"alwaysUseOSK":"None"
}
# - Object References -
var config : ConfigFile = ConfigFile.new() ## [1] Create a blank config file, this will be used for options. Note: The options menu isn't shown during regular gameplay, so set this node to only process whilst paused to save CPU. The config is laoded in memory at all times, though this may change as the game matures.
# NOTE: We don't use a seperate dictionary to store the working files, when we can just reference the keys directly from the ConfigFile as if it were a dictionary.
#endregion
#region = FUNCTIONS =

# - Boilerplate -

## Code executed when the game starts
func _ready():
	connectSignals()
	loadConfig()

# - Config Management -

## Set a value on the config. Changed by sliders, dropdowns, whatever, and live-updates everything. Note that configs are only saved with the save function. Also, the sliders and dropdowns from the options menu connect to setValue via a signal.
func setValue(_callVar : Variant, section : String, key : String, object: Control):
	var value : Variant = findValueFromObject(object)
	config.set_value(section,key,value)
	print("[OptionsManager] Setting value of " + key + " in section " + section + " to " + str(value))
	# TODO: Make a switch statement or some way to take the key and make it trigger the correct update function.

## Used for other objects to call to get a value from the options manager TODO: Implement
func getValue(_key: String):
	pass

## Used to restore defaults in multiple scenarios, returns the whole disctory, as every implementation of the defaults is to be done all at once.
func getDefaults() -> Dictionary:
	return defaultValues

## Used to pass the config file in it's entirety to the options menu, or any other script that needs all of the values in the config file.
## The Options Menu in particular needs this for the revert function, which is used to revert the sliders back to how they were when the config was last saved.
func getConfig() -> ConfigFile:
	return config

## Gets the config directly from disk for the sake of using the version from the last save. Used for reversions.
func getConfigFromDisk() -> ConfigFile:
	var diskConfig : ConfigFile = ConfigFile.new()
	diskConfig.load(configFileLocation) # Use disk config to get the config from disk without effecting the current values.
	return diskConfig

## A function that takes in a control object and then derives what kind of control it is in order to get a vlue.
func findValueFromObject(object: Control) -> Variant:
	var objectClass : String = object.get_class() # Get the class, we use this for the switch statement.
	match objectClass:
		"OptionButton":
			return object.get_item_text(object.get_selected_id())
		"HSlider":
			return object.value
	return 69420 # The funny error number

## Save config values to the config, called by the options menu.
func saveConfig():
	config.save(configFileLocation)

## Load configuration from default configuration file location. 
func loadConfig():
	var err = config.load(configFileLocation) # Load the current config from the defined location...
	if err != OK:
		print("Config file was invalid or could not be loaded!")
		return
	updateAllWorld()
	# TODO: When the config is loaded, update the world with the settings.

# - Update World -

# NOTE: When doing this, use ConfigFile.get_value() to unpack the current value from the config.
# For sections, use the names shown on in caps in the page buttons on the side of the options menu for the section. They are VIDEO, AUDIO, UI, OSK, and any other heading that is on that screen.
# For keys, Use the name as defined in the defaultValues to get the correct key value pair, they are based off the full names in the options menu, just in camelCase.
# Finally, for the defaults, always get them from the defaultValues dictionary, as they may change in updates or due to players unanimously wanting the defaults changed

## Update everything in the world, only to be used when all values are loaded, instead of when they are changed.
func updateAllWorld():
	pass

## Update the resolution, including showing a KDE Plasma / SRB2-style 'does this resolution look good' 15 second timeout screen.
func updateResolution():
	var _resolution : String = config.get_value("VIDEO","resolution",defaultValues)
	# TODO: Implement
 
# TODO: Implement, figure out a way to update only one thing at a time, maybe using a seperate method for each thing.

#endregion
#region = SIGNALS =

## Connect any and all signals required for the script to function
func connectSignals():
	pass

#endregion
# == REFERENCES ==
# [1] [https://docs.godotengine.org/en/stable/classes/class_configfile.html] <1207Fri26Apr24>
# [2] [https://docs.godotengine.org/en/stable/classes/class_dictionary.html] <1224Fri26Apr24>
