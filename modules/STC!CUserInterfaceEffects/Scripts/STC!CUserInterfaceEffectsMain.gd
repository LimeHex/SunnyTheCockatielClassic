extends Control
class_name STCCUserInterfaceEffectsMain

## STC!CUserInterfaceEffectsMain.gd - A module for the small caps rollover text, as well as any future UI effects that require states or signals.
## Jazztache - 1015Sat30Mar24

# NOTE: A lot of this work is just moved from ButtonEffects.gd, just so it is its own module. 
# It's also being rewritten for clarity and better documentation as well, since it never got a class name and got documented.
# It's also for all controls. I know it sounds like I am stretching to justify this but I have a hunch this is a good move, as it can be a dependency for other modules instead of the cursed game-specific code for a generic module.
# ...and I added strong typing, cleaned everything up and changed some variable names to make everything way more clear.

# == VARIABLES ==
# - Objects -
@export var rolloverEffectedText: RichTextLabel ## Effected by small caps when it's corresponding button is rolled over.
@export var rolloverControl: Control = self ## Triggers the rollover text to go into small caps. Usually this script is put on the object itself, but this can be changed if need be.
@onready var visibleParent: Node = get_parent() ## Effects how focus and visibility interact (Previously the parent in ButtonEffects.gd), automatically assigned to the object's parent, so that visibility changes are downpropogated.
# - Values -
@export var buttonText: String = "Please Set In Inspector" ## The text that is converted when applying it to the button.
@export var textSizePercent: float = 50 ## What percentage text should be sized in small caps mode.
@onready var textSizeDefault: float = getDefaultTextSize() ## What is the default size of the text? Used to calculate the size of the small caps text, so it's proportional relative to the normal text.
var isSmallCaps: bool ## Store wether we have already ran the small caps stuff.
var buttonTextCaps: String ## A storage variable so that hover text is cached at runtime and not recalculated.
# - Focus And Input Management
var focusGrabbed: bool ## Store wether or not [code]rolloverControl[/code] has it's focus grabbed
@export var tryGrabFocus = false ## Wether or not this control is trying to grab keyboard/controller focus
@export var focusIfParentChangedVisibility = false ## If [code]visibleParent[/code] changes visibility, grab focus. Used to indicate the first-to-highlight button objects on the pause menu, the win and loss screens. Good for when 'this is the first button'.

# == METHODS ==
# - Boilerplate -

func _ready():
	connectSignals() # Connect signals on ready.
	updateText(revertCaps()) # Center the text
	cacheSmallCapsString() # Calc and cache the small caps sting into a variable so we can use it later without doing the string formatting again.

## Default Godot function for unhandled input. We use unhandled input instead of just input because for some reason, it produces the desired effect. The desired effect is that when the button is pressed, it focuses the first item, and doesn't navigate or activate. Very nice. First press always JUST focuses. Experiemented around and found this and finally got the effect I wanted.
func _unhandled_input(event: InputEvent):
	# - Focus Grab On Prompt -
	if event.is_action_pressed("ui_focus_first"):
		tryFocus() # See if you can focus on rolloverControl

# - Object Updating -

## Update wether or not small caps are enabled. This is to make sure we don't double convert
func updateSmallCapsStatus(setCaps: bool):
	# - Compare -
	if setCaps == isSmallCaps:
		return # Ok, well everything is the same so nothing to do.
	if setCaps:
		updateText(smallCaps()) ## Edit the text on the button label to change from plain caps to full caps
	else:
		updateText(revertCaps()) ## Edit the text on the button label ok you know what its just the opposite of if it was true you can read the function name. 
	isSmallCaps = setCaps # Then we set the variable so it can be toggled at will.

## Using a signal without arguments, enable small caps.
func enableSmallCaps():
	updateSmallCapsStatus(true)

## Using a signal without arguments, DISable small caps.
func disableSmallCaps():
	updateSmallCapsStatus(false)

## Update the text on the button itself.
func updateText(newText):
	rolloverEffectedText.text = newText

# - Text Processing -

## Calculate and store the default text size, either based on theme or overrides. This function is kept around despite being one line and used once, because it's likely useful in future, and also so I can document how it worked.
func getDefaultTextSize() -> float:
	# NOTE: Just so you know, get_theme_font_size can also get the override. No idea how I kept on missing this. I was originally going to check if it was overridden, but then I added normal_font_)size and not font_size and it started working immeditely.
	return rolloverEffectedText.get_theme_font_size("normal_font_size") # Normal font sizes, also includes overrides from what I can tell. Took me ages to figure out that I needed this particular string.

## Cache the BBCode and text required for the small caps string
func cacheSmallCapsString():
	var displayText: String = buttonText
	var textSizeSmallCaps: float = textSizeDefault * (textSizePercent / 100)
	# - Convert Everything To Caps -
	displayText = displayText.to_upper() # Make it uppercase.
	# - Pick Out Individial Words -
	var displayTextSplits: Array = displayText.split(" ")
	displayText = "" # Reset display text, we are reconstructing it peice by peice.
	for stringSplit in displayTextSplits:
		# For each string...
		var allCaps: String = stringSplit.to_upper() # Make it uppercase.
		var firstLetter: String = allCaps.left(1) # Get the first letter [1]
		var everyOtherLetter: String = allCaps.right(allCaps.length() - 1) # Get all of the letters after that
		var builtString: String = firstLetter + "[font_size=" + str(textSizeSmallCaps) + "]" + everyOtherLetter + "[/font_size]" # The final string with the formatting. [2]
		displayText += builtString + " " # Add built string plus a space to the displaytext, so it builds out the full display string.
	displayText = displayText.left(displayText.length() - 1) # Cut off the last character, which is a trailing space.
	buttonTextCaps = displayText # Save it to the variable for use later on.

## Convert a "Title Like This" into a small caps cariant by captialisaing everything and using size tags.
func smallCaps() -> String:
	return "[center]" + buttonTextCaps + "[/center]"

## Convert small caps back into plain text, by removing the size tags.
func revertCaps() -> String:
	return "[center]" + buttonText + "[/center]" # Well... we already HAVE the display text. Just use that... but centered!

# - Focus -

## Try to focus the rolloverControl based on a few key factors, including visibility, already having focus and wether we want this button to actually grab focus.
func tryFocus():
	if focusIfParentChangedVisibility != null: # If we are using parent visibility...
		if !visibleParent.visible: # ...If the parent is not visible...
			return
	if focusGrabbed || !tryGrabFocus: # If focus was already grabbed, or we're not trying to grab focus
		return
	# Finally, try to grab focus if we didn't get filtered.
	grab_focus() # Godot's built-in method for grabbing focus
	focusGrabbed = true

## Reset the flag for if this has been focus. e.g.. Pause menu closed, or certain UI has been closed.
func resetFocus():
	if !focusIfParentChangedVisibility:
		return # No forced focus? Alright...
	if visibleParent.visible:
		focusGrabbed = false # Reset this flag if made visible.

# == SIGNALS ==

## Connect any required signals, such as rollover states, and resetting focus when one of the visible parents change
func connectSignals():
	# - Focus Visuals -
	rolloverControl.focus_entered.connect(enableSmallCaps) ## When focused/moused, send the signal for small caps to true.
	rolloverControl.mouse_entered.connect(enableSmallCaps)
	rolloverControl.focus_exited.connect(disableSmallCaps) ## See above but for normal caps
	rolloverControl.mouse_exited.connect(disableSmallCaps)
	# Look how they all end in nice neat lines. Cool!
	# - Try Focus -
	if focusIfParentChangedVisibility && visibleParent: # If we have a designated visibility parent, and they are visible.
		visibleParent.visibility_changed.connect(resetFocus) # If the parent's visibility changed, try to reset the focus state, which is needed in certain cases.
# == REFERENCES =
# - ButtonEffects.gd -
# [1] [https://docs.godotengine.org/en/stable/classes/class_string.html#class-string-method-left] <1544Tue20Feb24>
# [2] [https://docs.godotengine.org/en/stable/tutorials/ui/bbcode_in_richtextlabel.html] <1551Tue20Feb24>
# - STC!CUserInterfaceEffectsMain.gd -
# [3] [https://docs.godotengine.org/en/stable/classes/class_richtextlabel.html#class-richtextlabel-theme-font-size-normal-font-size] <1122Sat30Mar24>
# [4] [https://docs.godotengine.org/en/stable/classes/class_control.html#class-control-method-has-theme-font-override] <1147Sat30Mar24>
