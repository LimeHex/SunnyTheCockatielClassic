class_name STCCConsoleMain extends CanvasLayer

## STC!CConsoleMain.gd - The main script to control the console, which includes and info sent from and to the script.
## Jazztache - 1029Sun11Feb24

# == OBJECTS AND VARIABLES ==

# - Objects -
@onready var input : LineEdit = $Main/Input ## This is the main line for inputting text, included as a reference here.
@onready var outputSpawn : VBoxContainer = $Main/OutputSpawn ## This is the box that the instanced lines are displayed into.
var lineInstancer : PackedScene = load("res://modules/STC!CConsole/scenes/STC!COutputLineInstance.tscn") ## This is the object that is spawned to the output field.

# - Object/Hisotry Storage -
var lineStorage : Array ## This is the variable that allows you to store line output objects, just so they do not overflow.
var historyStorage : Array ## This is the array for storing all of the history. Useful for tabbing though. 

# TODO: Add autocomplete, and fish-style autocorrect.

# - Themeing -
var alternatePanelStylebox : StyleBoxFlat = load("res://modules/STC!CConsole/themes/STC!COutputOverrideGrey.tres") ## Alternate panel theme for each odd-numbered panel, is a slightly lighter shade of black/deep grey. Is a stylebox

# - Constants -
var MAX_ONSCREEN_LINES : int = 30 ## The maxiumum amount of lines onscreen. # TODO: Scale with screen size.

# - Variables -
var outputLineCount : int = 0 ## A counter for how many outputs have been processed, used for alternating line colours.
var currentHistorySelected : int = -1 ## A number that stores which entry of history to recall when the up and down buttons are used.

# - Debugging -
var debug_showConnections : bool = false ## Wether or not sending the connections of signals to the console is enabled. Sends in Array[Dictionary] format converted to string.

# - Cheats -
# TODO: Move this to game master, there might be a graphical way of enabling cheats!
var allowCheats : bool = false ## Wether or not cheating allowed, disallowing `tp` and `noclip`

# == METHODS ==

# - Boilerplate -

func _ready():
	connectSignals()
	# - Add To Group -
	add_to_group("console") # Add this to a group. consoles get added to this group, so other scripts know to look for this object there.

func _input(event):
	# - History Scrolling -
	if event.is_action_pressed("ui_up"):
		scrollHistory(1)
	if event.is_action_pressed("ui_down"):
		scrollHistory(-1)

# - Show And Hide -

#NOTE: show and hide console are likely to be moved to the module itself, just so it doesn't have to be re-implemented for future titles.

## Show the console onscreen
func showConsole():
	show()
	await get_tree().process_frame # process a frame and then grab focus.
	input.grab_focus() # [2]

## Hide the console from the screen
func hideConsole():
	hide()

# - Commands -

## NOTE: For these, it's still recommended to use signals just as an example so other devs (or future clueless me) can go back and reference the optimal way of doing this from other noes as well.
# For commands on other nodes: Please, create both the methods and signals in them, and use thier connectSignals function to connect the signals to the STC!CConsole

## Quit the game using a command.
func quitGame():
	get_tree().quit() # <1548Sun11Feb24>

# - Input Processing -

## Basic command processing
func processCommand(incoming_command : String):
	# - Process Inputs -
	var elements : PackedStringArray = incoming_command.split(" ") # Split up the elements for use with the signals.
	var output : String = ""
	var emission : Variant = "Unknown emission status..." # Can be an int or a string.
	var rootCMD : String = elements[0] # The root command is the initial text before the first space, basically kind of like which Bash program to call.
	var connectList : Array[Dictionary] = get_signal_connection_list(rootCMD) # The list of connections the node has. CRITICAL: The amount should be one, and one only. There is one console. It doesn't get deleted, it doesn't get respawned.
	var skipEmission: bool = false
	# - Pre-Checks -
	var cmdNotFound : String = "Unable to find command that matches [i]" + rootCMD + "[/i]... " # Command not found, no signal exists.
	if rootCMD == "": # If it's empty, just send an empty line
		send("")
		return
	if !has_signal(rootCMD):
		output = cmdNotFound
		skipEmission = true
	# - Emit Command Signals -
	# TODO: Later on you will have to define what is a command and what is not using a text file, make sure if a command is invalid, you show 'invalid command' on the console.
	# TODO: MAYBE: Instead of a switch cazse, find a way to turn a packedstringarray into something that can pass any number of args in. Like just removing the brackets and having the commas act as the arg boundaries. No clue how to get it working right now.
	# TODO: Add syntax highlighting.
		# Orange: incorrect command, but has valid completions
		# Red: incorrect command, but with no valid completions
		# Blue: Correct on it's own, also has completions.
		# Green: Correct, but with no more completions.
	# Maybe using the 'Callable' Function would be good.
	if !skipEmission:
		match elements.size(): # Find the number of args, and use the correct call. Later on, make something that can do the jobs of each case in one. Unless it's more performance expensive.
			1:
				emission = emit_signal(rootCMD) # Send out a signal to all objects listening for this command. They will parse it on thier end. The methods should accommodate both the command version and a raw call.
			2:
				emission = emit_signal(rootCMD,elements[1])
			3:
				emission = emit_signal(rootCMD,elements[1],elements[2]) # This needs to be updated to handle 2, and 4+ arguments, all without cases. Unless this method is unironically better performance-wise, then I guess 
			4:
				emission = emit_signal(rootCMD,elements[1],elements[2],elements[3]) # This is here to handle 4 args, but is only here for testing. Remember, later on (unless its more performance intensive), make something that can handle an arbritrary amount of commands.
			_:
				emission = 69 # The funniest of funny numbers, nice. Used for when we just DON'T support the amount of args. I recon keeping the matching might be good in this case.
		# - Emission ID Matching -
		# This switch case isn't a tie-over, new emmission values will be added her as the kinds of emissions emit_signal can make will be learned about. Handle as you go, and provide useful information when something is erroneously entered.
		match emission:
			0:
				pass # Do nothing, explicitly. Success!
			2:
				if has_signal(rootCMD): # Do we have a signal?
					if connectList.size() > 0: # Is the signal connected to anything
						output = "Signal for command [i]" + rootCMD + "[/i] is connected to " + str(connectList[0]) + " at a minimum however, it didn't fire to due emission error of 2, NOT_AVAILABLE"
					else: # if no connections...
						output = "Signal for command [i]" + rootCMD + "[/i] exists, but isn't connected internally. See if any nodes in this scene have the signal " + rootCMD + " connected." # Found a signal for the command, but it wasn't connected.
				else: # If there isn't even a signal...
					output = cmdNotFound
			37:
				output = "Found command of [i]" + rootCMD + "[/i]...but [b]" + str(elements.size()) + "[/b] arg(s) wasn't the exact amount necessary to run the command." # Command found, but wrong amount of args, too many or too little.
			69:
				output = "Sunny The Cockatiel! Classic doesn't support any functions with [b]four arguments and over[/b]. Try again with less args."
			_ :
				output = "Emission was unknown value of [b]" + str(emission) + "[/b]" # "Wait what????" kinda moment
	send(output) # Display output of command.
	if debug_showConnections:
		showConnectionsInConsole(rootCMD)
	# - Clean-Up / Wrap-Up -
	historyStorage.insert(0,incoming_command) # Append our input to the history storage, at the earliest possible position.
	clearLine() # Finally, clear the line.

# - Output Processing -

## Send the output of a command to the console
func send(display : String):
	# - Check for nothing -
	if display == "":
		return # <1605Sun11Feb24>
	# - Get Variables -
	var lineInstance = lineInstancer.instantiate() # Instatiate an output, then let's change it.
	var backplate = lineInstance.get_node("Backplate") # Get the backplate
	var outputText = lineInstance.get_node("Backplate/OutputText") # Get the outputText
	# - Set Variables -
	outputText.text = display # Set the Text
	if outputLineCount % 2 == 1: # Check if the line number is odd...
		backplate.add_theme_stylebox_override("panel", alternatePanelStylebox) # Change the panel to use the alternate slightly lighter grey panel theme. [1] [2]
		# NOTE: The 'panel' string is the type, scroll down on reference 2, on stackoverflow.
	# - Instance -
	outputSpawn.call_deferred("add_child",lineInstance) # Finally, instantiate it.
	# - Wrap-Up -
	lineStorage.append(lineInstance) # Add the line to the list, and...
	outputLineCount = outputLineCount + 1 # ...add one to the count! This makes every other line transparent.
	# - Cleaning -
	if lineStorage.size() > MAX_ONSCREEN_LINES: # If the lines are more than we can handle...
		var freedLine = lineStorage[0] # .. Find the oldest line spawned....
		lineStorage.pop_at(0) # .. take it out of the array so it isn't referenced again...
		freedLine.queue_free() # ... finally, free the object.
		# NOTE: You would usually use find for this, but because we always get index 0, it saves us time to not do this. <1130Sun11Feb24>

## Function for clearing the output. <1352Sun25Feb24>
func clearAll():
	var lineAmount = lineStorage.size() # Get the line amount
	for line in lineStorage:
		line.queue_free() # Free it.
	lineStorage.clear() # Clear lines
	send("Cleared [b]" + str(lineAmount) + "[/b] line(s) from the screen.")

## Show signals
func signalList():
	send("See console for list of connected signals, some trigger commands.")
	for signal_description in get_signal_list(): # [4]
		var signal_name := str(signal_description["name"])
		var connections := get_signal_connection_list(signal_name)
		for connection in connections:
			prints(connection)

# - History Processing -

## Used to scroll through the history, applying any movement to the selected historyNumber and retriving the existing history entry.
func scrollHistory(direction):
	# - Bailout -
	var size = historyStorage.size()
	if size == 0:
		return # No history? Then don't do no history.
	# - Direction -
	var newCurrentHistorySelected = currentHistorySelected # Copy the variable
	newCurrentHistorySelected = newCurrentHistorySelected + direction # Change it based on the number
	if newCurrentHistorySelected < -1: # If less than -1...
		newCurrentHistorySelected = -1 # ...then set to -1. -1 means no history, just blank.
	if newCurrentHistorySelected >= size: # Oh, we went too far back? Well...
		newCurrentHistorySelected = 0 # ...Around the world we go! Wraparound is a seldom used feature in terminals!
	# - Selection -
	if newCurrentHistorySelected == -1:
		pass # Do nothing, just keep the existing text...
	else: # Otherwise...
		input.set_text(historyStorage[newCurrentHistorySelected]) # Update the input with the history.
	# - Cleanup -
	currentHistorySelected = newCurrentHistorySelected # Update the value after everything has been checked

# - Utils -

# TODO: Later on, add in the option to display chat and kills as part of the console output, like in Source engine.

## Clears the line, removes all text from the input field.
func clearLine():
	input.clear()
	currentHistorySelected = -1 # Reset which history is selected

## Debugging variables, and support for the debug command.
func debugCommand(subcommand : String = "", option : String = ""): # [6]
	match subcommand:
		"":
			send("No valid subcommand, try `debug showConnections true` for example!")
		"showConnections":
			var old_debug_showConnections : bool = debug_showConnections
			match option:
				"on","true","1":
					debug_showConnections = true
					send("[b]Showing[/b] signal connections! Was: " + str(old_debug_showConnections))
				"off","false","0":
					debug_showConnections = false
					send("[b]Not[/b] showing signal connections. Was: " + str(old_debug_showConnections))
				"toggle":
					debug_showConnections = !debug_showConnections
					send("Toggled signal connections to [b]" + str(debug_showConnections) + "[/b]. Was: " + str(old_debug_showConnections))
				"show":
					send("Current value of debug_showConnections: [b]" + str(debug_showConnections))
				_:
					send("Usage: `debug showConnections [on/off/toggle/show]`. Current value: [b]" + str(debug_showConnections))
		_:
			send("Subcommand " + subcommand + " was not valid. try `debug showConnections` ")


##Allows you to toggle cheats on and off
func cheatsCommand(option : String):
	var old_allowCheats : bool = allowCheats
	match option:
				"on","true","1":
					allowCheats = true
					send("Cheating has been [b]enabled[/b]! Go nuts! High scores have been disabled for this run... Was: " + str(allowCheats))
				"off","false","0":
					allowCheats = false
					send("Cheating has been [b]disabled[/b]. Was: " + str(allowCheats))
				"toggle":
					allowCheats = !allowCheats
					send("Toggled cheating to [b]" + str(allowCheats) + "[/b]. Was: " + str(old_allowCheats))
				"show":
					send("Current value of allowCheats: [b]" + str(allowCheats))
				_:
					send("Usage: `cheats [on/off/toggle/show]`. Current value: [b]" + str(allowCheats))
# TODO: Add in ability to spawn in the text object.
# TODO: Add a script to the OutputLineInstance to make it fade and self-destroy over time.

## Return the current state of the 'allowCheats' value to any object that requests it. This is done by asking the console directly from the object. It's likely still stored from any initial signal connections.
func getCheatStateToObject() -> bool:
	return allowCheats

# == SIGNALS ==

signal quit() # The quit signal, if called from the console, allows you to quit.
signal close() # The close signal, useful for closing the console
signal noclip() # The noclip signal. Spawned players connect to this signal.
signal tp() # The teleport signal, used to teleport Sunny. Made with the assumption that STC!C is pure singleplayer. Needs reworking if multiplayer gets added in a post-launch update.
signal menu() # The menu signal, used to alter menu settings. <1317Sun25Feb24>
signal clear() # Clears the screen <1356Sun25Feb24>
signal signallist() # List all the connected signals to console for debugging <1450Sun30Jne24> Should be `debug signallist` in other games.
signal debug() # Change internal debugging flags and menus. 
signal cheats() # Toggle cheats on and off.
signal bing() # Get pinged idiot! Used for testing from scripts that seem to have issues with connecting signals. <1447Sun30Jne24>

## Connect signals, including a few basic ones like quit, clear and signallist.
func connectSignals():
	# - Other Nodes -
	input.text_submitted.connect(processCommand) # Submit linefeed's text to the method, this should include the string.
	# - Commands -
	## NOTE: This is also how you would do it from other nodes
	quit.connect(quitGame) # The signal is the name you type into the console, and the method is called something else
	clear.connect(clearAll)
	signallist.connect(signalList) # connect the lowercase signallist signal to the uppercase signalList function!
	debug.connect(debugCommand) # Connect the debug command, for debugging. Duh.
	cheats.connect(cheatsCommand) # Connect cheats command.
	### CRITICAL: NEVER, E V E R, use this method to connect game world stuff. The code is always in the game object, referencing the module, NOT the other way round. SUPER IMPORTANT you don't do that. This console's code is meant to be modular and work with other projects.

## Show what connections a signal has. Very useful in the case of objects not properly connecting on instantiation.
func showConnectionsInConsole(showMe : String):
	var output : String = "Cannot show connections..."
	var connIcon : String = "🯄"
	var connectList : Array[Dictionary] = get_signal_connection_list(showMe) # Get the connections list
	var connectListAmount : int = connectList.size()
	if has_signal(showMe):
		connIcon = "⇄" if connectListAmount > 0 else "✀" # Scissors as in 'oh... seems like the connection got cut
		output = connIcon + str(connectListAmount) + " [b]" + showMe + "[/b]" + str(connectList)  # The format for how the string is constructed.
	else:
		output = "🮖" + " [b]" + showMe + "[/b][i]<no signal>[/i]" 
	send(output) # Show all connections for current commands in STC!C Console Output
	print_rich(output) # Show all connections for current commands in Godot Console Output

# == REFERENCES ==
# [1] [https://docs.godotengine.org/en/stable/classes/class_theme.html] <1106Sun11Feb24>
# [2] [https://stackoverflow.com/a/76118239] <1110Sun11Feb24>
# [3] [https://forum.godotengine.org/t/set-node-position-from-node-2d/13939] <1320Fri16Feb24>
# [4] [https://gamedev.stackexchange.com/questions/204967/see-connected-signals-for-instantiated-node-while-in-debug-mode] <1310Sun30Jne24>
# [5] [https://stackoverflow.com/a/74970434] <1122Mon26Aug24>
# [6] [https://www.youtube.com/watch?v=30Dy8UlBllY] <2007Wed28Aug24>
# [7] [https://godottutorials.com/courses/introduction-to-gdscript/godot-tutorials-gdscript-09/] <2021Wed28Aug24>
