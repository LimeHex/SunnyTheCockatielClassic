class_name Template extends Node
## Template.gd - A new script
## Jazztache - HHMMdddDDmmmYY

#region = VARIABLES =
# - Values -

# - Object References -

#endregion
#region = FUNCTIONS =

# - Boilerplate -

## Code executed when the game starts
func _ready():
	connectSignals()

# - Actions -

#endregion
#region = SIGNALS =

## Connect any and all signals required for the script to function
func connectSignals():
	pass

#endregion
# == REFERENCES ==
# [1] [https://docs.godotengine.org/en/stable/tutorials/scripting/creating_script_templates.html] <1141Fri26Apr24>
